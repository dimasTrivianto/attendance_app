<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('register', 'userController@register');
/**
 * GOOGLE sign in
 */
Route::post('google-sign-in', 'userController@googleSignIn');
 /**
  * 
  */
Route::post('login/admin', 'userController@loginAdmin');
Route::post('login', 'userController@login');
Route::post('login/invitation', 'userController@loginInvitation');
Route::post('edit-profile', 'userController@editProfile')->middleware('jwt.verify');
Route::post('filter/member', 'userController@filterMember')->middleware('jwt.verify');
Route::post('clock-in', 'memberAttendanceController@store')->middleware('jwt.verify');
Route::post('clock-out', 'memberAttendanceController@update')->middleware('jwt.verify');
Route::post('/check/clock-in', 'memberAttendanceController@check')->middleware('jwt.verify');
Route::get('/company/today-attendance', 'memberAttendanceController@companyTodayAttendance')->middleware('jwt.verify');
Route::post('/company/date-attendance', 'memberAttendanceController@companyDateAttendance')->middleware('jwt.verify');
Route::get('/company/attendance/{month}/{year}', 'memberAttendanceController@companyAttendance')->middleware('jwt.verify');
Route::post('upload/photos','userController@uploadPhotos')->middleware('jwt.verify');
Route::get('user-attendance/{month}/{year}','memberAttendanceController@getUserAttendance')->middleware('jwt.verify');
Route::post('user-attendance/byMember','memberAttendanceController@attendanceByMember')->middleware('jwt.verify');
Route::get('user','userController@getUserData')->middleware('jwt.verify');
Route::get('user/profile','userController@getUserProfile')->middleware('jwt.verify');
Route::get('user/admin','userController@getUserAdmin')->middleware('jwt.verify');
Route::post('update/user/profile','userController@updateUserProfile')->middleware('jwt.verify');
Route::post('delete/user/profile','userController@deleteUserProfile')->middleware('jwt.verify');
Route::post('schedule/create','meetingSchedule@store')->middleware('jwt.verify');
Route::post('schedule/update','meetingSchedule@edit')->middleware('jwt.verify');
Route::post('schedule/delete','meetingSchedule@delete')->middleware('jwt.verify');
Route::get('schedule/detail/{id}','meetingSchedule@detailSchedule')->middleware('jwt.verify');
Route::post('schedule/response','meetingSchedule@update')->middleware('jwt.verify');
//Get invitation member on schedule
Route::get('all-user','meetingSchedule@getAllUser')->middleware('jwt.verify');

Route::get('schedule/all','meetingSchedule@getAllSchedule')->middleware('jwt.verify'); //Bug
Route::get('schedule/all/{date}','meetingSchedule@getScheduleBydate')->middleware('jwt.verify');
Route::get('schedule/unresponse','meetingSchedule@unresponseSchedule')->middleware('jwt.verify');
Route::get('schedule/accepted','meetingSchedule@acceptedSchedule')->middleware('jwt.verify');
Route::get('schedule/rejected','meetingSchedule@rejectedSchedule')->middleware('jwt.verify');
Route::get('user-attendance/{date}','memberAttendanceController@getUserAttendanceByday')->middleware('jwt.verify');

Route::post('overtime','memberAttendanceController@overtimePost')->middleware('jwt.verify');
Route::get('overtime','memberAttendanceController@overtimeGet')->middleware('jwt.verify');
Route::get('company/overtime','memberAttendanceController@companyOvertime')->middleware('jwt.verify');
Route::post('company/update/overtime','projectController@updateOvertime')->middleware('jwt.verify');

Route::get('leave-type','leaveController@leaveType')->middleware('jwt.verify');
Route::post('leave','leaveController@store')->middleware('jwt.verify');
Route::get('leave','leaveController@index')->middleware('jwt.verify');
Route::get('leave/user','leaveController@userLeave')->middleware('jwt.verify');
Route::post('leave/user','leaveController@updateUserLeave')->middleware('jwt.verify');
Route::get('leave/per-user','leaveController@perUserLeave')->middleware('jwt.verify');
Route::get('leave/per-user/no','leaveController@perUserLeaveNo')->middleware('jwt.verify');
Route::post('change/user-leave/no','leaveController@changeUserLeaveNo')->middleware('jwt.verify');
Route::post('change/all-leave/no','leaveController@changeAllLeaveNo')->middleware('jwt.verify');
Route::get('leave/taken','leaveController@userLeaveTaken')->middleware('jwt.verify');
// Route::post('change/earned/no','leaveController@changeEarnedNo')->middleware('jwt.verify');

Route::get('team-status', 'memberAttendanceController@teamStatus')->middleware('jwt.verify');
Route::post('add-new/teams', 'teamController@teamNew')->middleware('jwt.verify');
Route::post('add-new/designation', 'teamController@designationNew')->middleware('jwt.verify');

Route::get('user-company', 'userController@company')->middleware('jwt.verify');
Route::get('currency', 'companyController@currency')->middleware('jwt.verify');
Route::get('language', 'companyController@language')->middleware('jwt.verify');
Route::post('edit/user-company', 'companyController@companyEdit')->middleware('jwt.verify');
Route::get('profile-changes/notification', 'userController@profileChangesNotification')->middleware('jwt.verify');
Route::post('update/profile-notification', 'userController@updateProfileNotification')->middleware('jwt.verify');

Route::get('company/members', 'companyController@companyMembers')->middleware('jwt.verify');
// Route::get('company/code', 'companyController@companyAllCode');
Route::get('company-code/{code}', 'companyController@companyCode');
Route::post('company-code', 'companyController@requestCompany')->middleware('jwt.verify');
Route::post('company-code-web', 'companyController@requestCompanyWeb')->middleware('jwt.verify');
Route::post('company/acceptance', 'companyController@acceptUser')->middleware('jwt.verify'); //Route accept user masuk company oleh HRD
Route::get('company/request', 'companyController@companyRequest')->middleware('jwt.verify'); //Route untuk cek list user request join company
Route::get('company/department', 'companyController@companyDepartment')->middleware('jwt.verify');
Route::get('company/designation', 'companyController@companyDesignation')->middleware('jwt.verify');
Route::post('company/delete/designation', 'companyController@deleteDesignation')->middleware('jwt.verify');
Route::post('company/delete/department', 'companyController@deleteDepartment')->middleware('jwt.verify');
Route::post('company/manage/designations', 'companyController@manageDesignation')->middleware('jwt.verify');
Route::post('company/manage/department', 'companyController@manageDepartment')->middleware('jwt.verify');
Route::post('change/role', 'companyController@changeRole')->middleware('jwt.verify');
Route::post('change/department', 'companyController@changeDepartment')->middleware('jwt.verify');
Route::post('change/designation', 'companyController@changeDesignation')->middleware('jwt.verify');
Route::post('team', 'teamController@store')->middleware('jwt.verify');
Route::get('roles', 'teamController@roles');
Route::get('user/roles', 'teamController@userRoles')->middleware('jwt.verify');
Route::get('team/{id}', 'teamController@detail');
Route::get('team-per-user', 'teamController@teamPerUser')->middleware('jwt.verify');

Route::post('organization', 'Organizationcontroller@store')->middleware('jwt.verify');
Route::get('plans', 'Organizationcontroller@plans');
Route::get('businessScope', 'Organizationcontroller@businessScope');

Route::post('/submit/forgot-email', 'forgotPassController@submitEmail');
Route::post('/submit/reset-pass', 'forgotPassController@store');


Route::get('/all/finished/project', 'projectController@finishedProject')->middleware('jwt.verify');
Route::get('/all/project', 'projectController@allProject')->middleware('jwt.verify');
Route::get('/all/active/project', 'projectController@allActiveProject')->middleware('jwt.verify');
Route::post('/project', 'projectController@store')->middleware('jwt.verify');
Route::get('/project', 'projectController@index')->middleware('jwt.verify');
Route::get('/project/notStarted', 'projectController@notStarted')->middleware('jwt.verify');
Route::get('/project/user', 'projectController@projectPerUser')->middleware('jwt.verify');
Route::get('/project/all/unfinished', 'projectController@allUnfinished')->middleware('jwt.verify');

Route::post('/label', 'projectController@createLabel')->middleware('jwt.verify');
Route::get('/label', 'projectController@getLabel')->middleware('jwt.verify');

Route::get('/overdue/task', 'projectController@overdueTask')->middleware('jwt.verify');
Route::get('/overdue/project', 'projectController@overdueProject')->middleware('jwt.verify');
Route::post('/task', 'projectController@createTask')->middleware('jwt.verify');
Route::put('/task', 'projectController@editTask')->middleware('jwt.verify');
Route::put('/task/progress', 'projectController@taskProgress')->middleware('jwt.verify');
Route::get('/task', 'projectController@getTask')->middleware('jwt.verify');
Route::get('/task/user', 'projectController@taskPerUser')->middleware('jwt.verify');
Route::get('/task/user/done', 'projectController@taskDonePerUser')->middleware('jwt.verify');
Route::get('/comment', 'projectController@getComment')->middleware('jwt.verify');
Route::post('/comment', 'projectController@createComment')->middleware('jwt.verify');

Route::post('/checklist', 'projectController@createChecklist')->middleware('jwt.verify');
Route::put('/checklist', 'projectController@editChecklist')->middleware('jwt.verify');
Route::put('/checklist/edit', 'projectController@editChecklistName')->middleware('jwt.verify');
Route::get('/checklist', 'projectController@getChecklist')->middleware('jwt.verify');
Route::delete('/checklist', 'projectController@deleteChecklist')->middleware('jwt.verify');

Route::post('/attachment', 'projectController@attach')->middleware('jwt.verify');
Route::get('/attachment', 'projectController@getAttachment')->middleware('jwt.verify');

Route::post('forgot-password', 'mailController@sendMail');
Route::post('forgot-password/create', 'userController@forgotPassword');

Route::resource('bookmark', 'bookmarkController')->middleware('jwt.verify');

Route::get('/new-access', 'userController@getNewAccess')->middleware('jwt.verify');
Route::post('/new-access', 'userController@updateNewAccess')->middleware('jwt.verify');
Route::post('/new-employee', 'userController@addNewEmployee')->middleware('jwt.verify');
Route::get('/hours-log', 'memberAttendanceController@hoursLog')->middleware('jwt.verify');
Route::get('/attendance-setting', 'memberAttendanceController@attendanceSetting')->middleware('jwt.verify');
Route::post('/edit/attendance-setting', 'memberAttendanceController@editAttendanceSetting')->middleware('jwt.verify');
Route::post('/change-picture/{user_id}', 'userController@changePicture')->middleware('jwt.verify');

Route::post('/notif', 'userController@notif')->middleware('jwt.verify');
Route::get('/notif-clockOut', 'userController@notifClockOut')->middleware('jwt.verify');
Route::get('/message-type', 'processcontroller@messageType')->middleware('jwt.verify');
Route::get('/contact-supervisor/{id}', 'processcontroller@getContactSupervisor')->middleware('jwt.verify');
Route::post('/contact-supervisor', 'processcontroller@contactSupervisor')->middleware('jwt.verify');
Route::get('/getMessageSupervisor', 'processcontroller@getMessageSupervisor')->middleware('jwt.verify');
Route::delete('/dltContactSupervisor/{id}', 'processcontroller@dltContactSupervisor')->middleware('jwt.verify');
Route::get('/all-notif', 'processcontroller@notif')->middleware('jwt.verify');
Route::get('/today-meeting-notif', 'processcontroller@todayMeetingNotif')->middleware('jwt.verify');
