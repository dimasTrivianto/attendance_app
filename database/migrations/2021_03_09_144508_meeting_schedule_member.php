<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MeetingScheduleMember extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_member', function (Blueprint $table) {
            $table->id();
            $table->integer('scheduleId');
            $table->integer('userId');
            $table->boolean('accepted')->nullable();
            $table->string('rejectedReason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_member');
    }
}
