<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewDataOnOvertime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('overtime_work', function (Blueprint $table) {
            $table->integer('company_id')->nullable();
            $table->date('date');
            $table->time('startTime')->nullable();
            $table->time('endTime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('overtime_work', function (Blueprint $table) {
            //
        });
    }
}
