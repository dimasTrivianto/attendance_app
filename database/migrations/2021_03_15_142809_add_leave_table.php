<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeaveTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->integer('user_id');
            $table->integer('leave_type_id');
            $table->enum('duration', ['Single Day', 'Multiple Day', 'Half Day']);
            $table->date('leave_date');
            $table->time('startTime')->nullable();
            $table->time('endTime')->nullable();
            $table->string('reason');
            $table->text('reject_reason')->nullable();
            $table->enum('status', ['approved', 'pending', 'rejected']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
