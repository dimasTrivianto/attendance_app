<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('plan_id');
            $table->boolean('attendance');
            $table->boolean('task_management');
            $table->enum('project_limit', array('unlimited', '3'));
            $table->boolean('project_role');
            $table->boolean('meeting_scheduling');
            $table->boolean('meeting_role_assign');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans_detail');
    }
}
