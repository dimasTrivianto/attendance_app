<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactSupervisorDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_supervisor_detail', function (Blueprint $table) {
            $table->id();
            $table->longText('message');
            $table->integer('company_id');
            $table->integer('message_type_id');
            $table->string('about');
            $table->boolean('status')->default(0);
            $table->integer('message_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_supervisor_detail');
    }
}
