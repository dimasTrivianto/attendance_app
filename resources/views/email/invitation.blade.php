<!DOCTYPE html>
<html>

<head>
    <title>Zukses Invitation Link</title>
</head>

<body>
    {{-- <img src="{{ $message->embed(asset('images/email_picture.jpeg')) }}"> --}}

    <h3>Hi,</h3>

    <p>you have been invited to join team {{ $details['team_name'] }}</p>
    <p>Please click this link to join with your team members</p>
    <p>{{ $details['link'] }}</p>
</body>

</html>
