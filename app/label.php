<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class label extends Model
{
    protected $table = 'task_label';
    
    protected $guarded = [];
    
    public function task(){
        return $this->hasMany(\App\task::class,'label_id');
    }
    
}
