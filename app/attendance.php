<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Scopes\CompanyScope;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;
use Illuminate\Support\Facades\Auth;
class attendance extends Model
{
    //
    // protected $dates = ['clock_in_time', 'clock_out_time'];
    // protected $appends = ['clock_in_date'];
    protected $guarded = ['id'];

    protected static function boot()
    {
        parent::boot();

        // static::observe(AttendanceObserver::class);

        // static::addGlobalScope(new CompanyScope);
    }
        // Get User Clock-ins by date
        public static function getTotalUserClockIn($date, $userId){
            return Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $date)
                ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $date)
                ->where('user_id', $userId)
                ->count();
        }

        public function getClockInDateAttribute()
        {
            // $global = Company::withoutGlobalScope('active')->where('id', Auth::user()->company_id)->first();
            // return $this->clock_in_time->timezone($global->timezone)->toDateString();
        }

        public static function getAttendanceById($id){
            return Attendance::where('id',$id)->first();
        }

        public static function getAttendanceByUserId($userId){
            return Attendance::where('user_id',$userId)->where('clock_out_time',NULL)->orderBy('id','desc')->first();
        }

        public static function todayAttendance($id){
            $now = Carbon::now();
            $date =$now->format('Y-m-d');
            $todayAttendance = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $date)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), '<=', $date)
            ->where('user_id', $id)
            ->count();
            if($todayAttendance!=0){
                $data = Attendance::where('user_id',$id)->orderBy('id','desc')->first();
                if($data->clock_out_time == NULL){
                    return 'true';
                }
                else{
                    return 'false';
                }
            }
            else{
                return 'false';
            }

        }
        public static function getUserAttendance($userId, $month, $year){
            $data = \DB::table('attendances')->select('id','clock_in_time','clock_out_time', 'late', 'created_at')
            ->where('user_id',$userId)
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month)
            ->get();

            $companiesId = \DB::table('users')->where('id', $userId)->pluck('company_id')->first();
            $companies = \DB::table('companies')->where('id', $companiesId)->first();
           
            foreach($data as $i => $val){
                 $officeStartTime = \DB::table('attendance_settings')->where('company_id', $companiesId)->pluck('office_start_time')->first();
            $officeStartTime = date("H:i:s", strtotime($officeStartTime));
            $officeEndTime = \DB::table('attendance_settings')->where('company_id', $companiesId)->pluck('office_end_time')->first();
              $val->clock_in_time =  Carbon::parse($val->clock_in_time, 'UTC')->setTimezone( $companies->timezone)->format('Y-m-d H:i:s');
              if($val->clock_out_time == null){
              $val->clock_out_time =  null;
                  
              }else{
              $val->clock_out_time =  Carbon::parse($val->clock_out_time, 'UTC')->setTimezone( $companies->timezone)->format('Y-m-d H:i:s');
                  
              }
            //   Carbon::parse($val->clock_in_time, 'UTC')->setTimezone( $companies->timezone);
              $clockIn = date("H:i:s", strtotime($val->clock_in_time));
              $clockOut = date("H:i:s", strtotime($val->clock_out_time));
            //   return $clockIn;
              $data[$i]->officeStartTime = $officeStartTime;
              $data[$i]->officeEndTime = $officeEndTime;
              
              
              $clockIn = strtotime($clockIn);
              $officeStartTime = strtotime($officeStartTime);
              $minuteLate = $clockIn - $officeStartTime;
              
              $clockOut = strtotime($clockOut);
              $officeEndTime = strtotime($officeEndTime);
              
              $minuteOvertime = $clockOut - $officeEndTime;
              
              if($minuteOvertime > 0){
                $minuteOvertime = gmdate('H:i:s', $clockOut - $officeEndTime);
                // $minuteLate = date("Y-m-d H:i:s", $minuteLate);
                $data[$i]->minuteOvertime = $minuteOvertime;
              }
              else{
                  $data[$i]->minuteOvertime = NULL;
              }
              
              if($minuteLate > 0){
                $minuteLate = gmdate('H:i:s', $clockIn - $officeStartTime);
                // $minuteLate = date("Y-m-d H:i:s", $minuteLate);
                $data[$i]->minuteLate = $minuteLate;
              }
              else{
                  $data[$i]->minuteLate = NULL;
              }
          }
           return $data;
        }
        
        public static function getUserAttendanceByDays($date){
            $user = Auth::user();
            $userId = $user->id;
            $data = \DB::table('attendances')->select('id','clock_in_time','clock_out_time', 'late', 'created_at')
            ->where('user_id',$userId)
            ->whereDate('created_at', '=', $date)
            ->get();

            $companiesId = \DB::table('users')->where('id', $userId)->pluck('company_id')->first();
            $companies = \DB::table('companies')->where('id', $companiesId)->first();
          foreach($data as $i => $val){
              $val->clock_in_time =  Carbon::parse($val->clock_in_time, 'UTC')->setTimezone( $companies->timezone)->format('Y-m-d H:i:s');
              $val->clock_out_time =  Carbon::parse($val->clock_out_time, 'UTC')->setTimezone( $companies->timezone)->format('Y-m-d H:i:s');
            //   Carbon::parse($val->clock_in_time, 'UTC')->setTimezone( $companies->timezone);
          }
           return $data;
        }
        
        // public function setTimeAttribute($value){    
        //         return $this->clock_in_time->timezone($value)->toDateString();
        // }
        protected static function teamAttendance(){
            $user = Auth::user();
            $userId = $user->id;
            $companyId = Auth::user()->company_id;
            $companyTimezone = \DB::table('companies')->where('id', $companyId)->first()->timezone;
            $teamId = \DB::table('employee_teams')->where('user_id',$userId)->first()->team_id;
            $now = Carbon::now()->format('Y-m-d');
            $attendance = \DB::table('attendances')
                          ->select('users.id as userId', 'users.name', 'users.email', 'users.image', 'attendances.clock_in_time','attendances.clock_out_time', 'attendances.late', 'attendances.created_at')
                          ->rightJoin('employee_teams', 'employee_teams.user_id', 'attendances.user_id')
                          ->where('employee_teams.team_id', $teamId)
                          ->where(function ($query) use ($now) {
                                $query->whereDate('attendances.clock_in_time', $now)
                                      ->orWhere('attendances.clock_in_time', NULL);
                            })
                            ->where('users.id','!=',$userId)
                          ->rightJoin('users', 'users.id', 'employee_teams.user_id')
                          ->get();
            foreach($attendance as $key=>$value){
                // \Carbon\Carbon::parse($attendances->clock_in_time, 'UTC')->setTimezone( $company->timezone)->format('H:i'), 
                if($value->clock_in_time != null){
                    $attendance[$key]->clock_in_time = \Carbon\Carbon::parse($value->clock_in_time, 'UTC')->setTimezone( $companyTimezone)->format('Y-m-d H:i:s');
                }
                if($value->clock_out_time != null){
                    $attendance[$key]->clock_out_time = \Carbon\Carbon::parse($value->clock_out_time, 'UTC')->setTimezone( $companyTimezone)->format('Y-m-d H:i:s');
                    
                }
            }
            return $attendance;
        }
        
        protected static function companyAttendance(){
            $user = Auth::user();
            $userId = $user->id;
            $company = \DB::table('users')->where('users.id',$userId)->join('companies', 'companies.id', 'users.company_id')->select('users.company_id', 'companies.timezone')->first();
            $companyId = $company->company_id;
            // return $userId;
            $now = Carbon::now()->format('Y-m-d');
            $companyMembers = \DB::table('users')->where('company_id', $companyId)->get();
            // $attendances = \DB::table('attendances')
            //               ->select('users.id as userId', 'users.name', 'users.email', 'users.image', 'attendances.clock_in_time','attendances.clock_out_time', 'attendances.late', 'attendances.created_at')
            //               ->rightJoin('users', 'users.id', 'attendances.user_id')
            //               ->where('users.company_id', $companyId)
            //               ->where(function ($query) use ($now) {
            //                     $query->whereDate('attendances.clock_in_time', $now);
            //                         //   ->orWhere('attendances.clock_in_time', NULL);
            //                 })
            //               ->first();
                // return $companyMembers;
                $companyAttendanceData = null;
            foreach ($companyMembers as $member){
                $attendances = \DB::table('attendances')
                          ->select('users.id as userId', 'users.name', 'users.email', 'users.image', 'attendances.clock_in_time','attendances.id AS attendanceId','attendances.clock_out_time', 'attendances.late', 'attendances.created_at')
                          ->rightJoin('users', 'users.id', 'attendances.user_id')
                          ->where(function ($query) use ($now) {
                                $query->whereDate('attendances.clock_in_time', $now);
                                    //   ->orWhere('attendances.clock_in_time', NULL);
                            })
                            ->where('users.id', $member->id)
                          ->first();
                // return $attendances;
                if($attendances != null){
                    // return $attendances[0]->userId;
                    $companyAttendanceData[] =[
                    'userId'=>$attendances->userId,    
                    'name'=>$attendances->name,    
                    'email'=>$attendances->email,    
                    'image'=>$attendances->image,    
                    'clock_in'=>$attendances->clock_in_time,
                    'clock_out'=>$attendances->clock_out_time,
                    'timeZone'=>$company->timezone,
                    'late'=>$attendances->late,    
                    'created_at'=>$attendances->created_at,
                    'attendanceId'=>$attendances->attendanceId,
                    'companyId'=>$companyId,
                    ];
                }else{
                    // $companyAttendanceData = null;
                }
                  
            }
            $data = collect($companyAttendanceData)->toArray();
            return $data;
        }
        protected static function companyDateAttendance($date){
            $user = Auth::user();
            // return $user;
            $userId = $user->id;
            $company = \DB::table('users')->where('users.id',$userId)->join('companies', 'companies.id', 'users.company_id')->select('users.company_id', 'companies.timezone')->first();
            $companyId = $company->company_id;
            $date = $date;
            $companyMembers = \DB::table('users')->where('company_id', $companyId)->get();
            $companyAttendanceData = [];
            foreach ($companyMembers as $member){
            // return $member;
                
                $attendances = \DB::table('attendances')
                          ->select('users.id as userId', 'users.name', 'users.email', 'users.image', 'attendances.clock_in_time','attendances.id AS attendanceId','attendances.clock_out_time', 'attendances.late', 'attendances.created_at', 'attendances.clock_in_ip', 'clock_out_ip', 'attendances.working_from')
                          ->rightJoin('users', 'users.id', 'attendances.user_id')
                          ->where(function ($query) use ($date) {
                                $query->whereDate('attendances.clock_in_time', $date);
                                    //   ->orWhere('attendances.clock_in_time', NULL);
                            })
                            ->where('users.id', $member->id)
                          ->first();
                // return $attendances;
                if($attendances != null && $attendances->clock_out_time == null){
                    // return $attendances[0]->userId;
                    $companyAttendanceData[] =[
                    'userId'=>$attendances->userId,    
                    'name'=>$attendances->name,    
                    'email'=>$attendances->email,    
                    'image'=>$attendances->image,    
                    'clock_in'=>\Carbon\Carbon::parse($attendances->clock_in_time, 'UTC')->setTimezone( $company->timezone)->format('H:i'),    
                    'clock_out'=>null,
                    'clock_out_ip'=>null,
                    'clock_in_ip'=>$attendances->clock_in_ip,
                    'working_from'=>$attendances->working_from,
                    'late'=>$attendances->late,    
                    'created_at'=>$attendances->created_at,
                    'attendanceId'=>$attendances->attendanceId,
                    ];
                }elseif($attendances != null && $attendances->clock_out_time != null){
                    $companyAttendanceData[] =[
                    'userId'=>$attendances->userId,    
                    'name'=>$attendances->name,    
                    'email'=>$attendances->email,    
                    'image'=>$attendances->image,    
                    'clock_in'=>\Carbon\Carbon::parse($attendances->clock_in_time, 'UTC')->setTimezone( $company->timezone)->format('H:i'),    
                    'clock_out'=>\Carbon\Carbon::parse($attendances->clock_out_time, 'UTC')->setTimezone( $company->timezone)->format('H:i'), 
                    'clock_out_ip'=>$attendances->clock_out_ip,
                    'clock_in_ip'=>$attendances->clock_in_ip,
                    'working_from'=>$attendances->working_from,
                    'late'=>$attendances->late,    
                    'created_at'=>$attendances->created_at,
                    'attendanceId'=>$attendances->attendanceId,
                    ];
                }else{
                    // $companyAttendanceData = null;
                }
                  
            }
            $data = collect($companyAttendanceData)->toArray();
            return $data;
        }
        protected static function maxClockIn($id){
            $now = Carbon::now();
            $date =$now->format('Y-m-d');
            $todayAttendance = Attendance::where(DB::raw('DATE(attendances.clock_in_time)'), '>=', $date)
            ->where(DB::raw('DATE(attendances.clock_out_time)'), '!=', NULL)
            ->where(DB::raw('DATE(attendances.clock_in_time)'), $date)
            ->where('user_id', $id)
            ->count();
            $companyId = DB::table('users')->select('company_id')->where('id',$id)->first()->company_id;
            $maxClockInCompany = DB::table('attendance_settings')->where('company_id',$companyId)->first()->clockin_in_day;
            if($todayAttendance < $maxClockInCompany){
                return 'false';
            }
            else{
                return 'true';
            }
        }
        
        public static function hourLog($userId){
            $current_time = date('Y-m-d');
            $hourLogged = \DB::table('attendances')->where('user_id', $userId)->whereDate('clock_in_time', '<', $current_time)->where('clock_out_time', '!=', null)->get();
            $hourLog = 0;
            foreach($hourLogged as $key=>$hour){
                $startHour = Carbon::createFromFormat('Y-m-d H:i:s', $hour->clock_in_time);
                $endHour = Carbon::createFromFormat('Y-m-d H:i:s', $hour->clock_out_time);
                $logHour = $startHour->diffInHours($endHour);
                $hourLog = $hourLog + $logHour;
            }
            return $hourLog;
        }
        
        public static function companyAllAttendances(){
            $companyId = Auth::user()->company_id;
            $getCompanyAttendance = \DB::table('attendances')->where('company_id', $companyId)->where('clock_out_time', '!=', null)->select('user_id', 'clock_in_time', 'clock_out_time')->get();
            return $getCompanyAttendance;
        }
        public static function companyAllAttendancesPerUser($month, $year){
            $companyId = Auth::user()->company_id;
            $getcompanyMember = \DB::table('users')->where('company_id', $companyId)->get();
            $selectedDate = "$year-$month-01";
            $dateNumber = date('t', strtotime($selectedDate));
            $days = [];
            for($i = 0; $i<$dateNumber; $i++){
                $datetime = new \DateTime($selectedDate.'+'.$i.'days');
                $dateName = $datetime->format('D');
                $days[] = [
                    'daysName'=>$dateName,
                    'date'=>$i+1,
                ];
                //  $getAttendanceData = \DB::table('attendances')->whereDate('clock_in_time', '2021-06-10')->where('user_id', 199)->first();
                //  return $getAttendanceData;
            }
                $attendances = [];
                foreach($getcompanyMember as $key=>$value){
                $attendances[$value->name] = [];
                    for($i = 0; $i<$dateNumber; $i++){
                        $datetime = new \DateTime($selectedDate.'+'.$i.'days');
                        $dateName = $datetime->format('D');
                        if($dateName == 'Sun' || $dateName == 'Sat'){
                            // $attendances [$value->name] = [
                            // 'name'=> $value->name,
                            // 'status'=> 'Absent',
                            // ];
                            array_push($attendances[$value->name], 'Holiday');
                        }else{
                            $getAttendanceData = \DB::table('attendances')->whereDate('clock_in_time', $datetime)->where('user_id', 199)->first();
                            
                            if($getAttendanceData != null){
                                array_push($attendances[$value->name], 'Present');
                            }else{
                                array_push($attendances[$value->name], 'Absent');
                            }
                        
                        }

                    }
                }
            // return $days;
            return [$attendances,$days];
        }
        public static function getUserAttendanceByMember($id, $startDate, $endDate){
            $companyId = Auth::user()->company_id;
            $timezone = \DB::table('companies')->where('id', $companyId)->pluck('timezone')->first();
            $datetime1 = new \DateTime($startDate);
            $datetime2 = new \DateTime($endDate);
            $interval = $datetime1->diff($datetime2);
            $days = $interval->format('%a');
            $attendanceData = [];
            // $tes = Carbon::parse($startDate)->dayOfWeek;
            for($i = 0; $i<$days; $i++){
                $date = new \DateTime($startDate.'+'.$i.'days');
                $dateTime = $date->format('d-m-yy');
                $dateName = $date->format('l');
                if($dateName == 'Sunday' || $dateName == 'Saturday'){
                    $attendanceData[] = [
                            'days'=>$dateName,
                            'status'=>'Holiday',
                            'clock_in'=>'-',
                            'clock_out'=>'-',
                            'clock_in_ip'=>'-',
                            'clock_out_ip'=>'-',
                            'work_from'=>'-',
                            'date'=>$dateTime,
                            'daysLate' => 'no',
                            'halfDay' => 'no'
                        ];
                }else{
                    $getAttendanceData = \DB::table('attendances')->where('user_id', $id)->whereDate('clock_in_time', $date)->first();
                    if($getAttendanceData != null){
                        $attendanceData[] = [
                            'days'=>$dateName,
                            'status'=>'Present',
                            'clock_in'=>Carbon::parse($getAttendanceData->clock_in_time, 'UTC')->setTimezone( $timezone)->format('H:i'),
                            'clock_out'=>Carbon::parse($getAttendanceData->clock_out_time, 'UTC')->setTimezone( $timezone)->format('H:i'),
                            'clock_in_ip'=>$getAttendanceData->clock_in_ip,
                            'clock_out_ip'=>$getAttendanceData->clock_out_ip,
                            'work_from'=>$getAttendanceData->working_from,
                            'date'=>$dateTime,
                            'daysLate' => $getAttendanceData->late,
                            'halfDay' => $getAttendanceData->half_day
                        ];
                    }else{
                        $attendanceData[] = [
                            'days'=>$dateName,
                            'status'=>'Absent',
                            'clock_in'=>'-',
                            'clock_out'=>'-',
                            'clock_in_ip'=>'-',
                            'clock_out_ip'=>'-',
                            'work_from'=>'-',
                            'date'=>$dateTime,
                            'daysLate' => 'no',
                            'halfDay' => 'no'
                        ];
                    }
                }
            }
            
            return $attendanceData;
        }
}
