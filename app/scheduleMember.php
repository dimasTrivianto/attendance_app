<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class scheduleMember extends Model
{
    //
    protected $table = "meeting_member";
    protected $guarded = ['id'];
    public $timestamps = false;

    public static function getMeetingById($meetingId){
        $user = Auth::user();
        $userId = $user->id;
        return scheduleMember::where('userId', $userId)->where('scheduleId', $meetingId)->first();
    }
}
