<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task_checklist extends Model
{
    protected $table = 'task_checklist';
    
    protected $guarded = ['id'];
    
    public function task(){
        return $this->belongsTo(\App\task::class,'task_id');
    }
}
