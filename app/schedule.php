<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\scheduleMember;
class schedule extends Model
{
    //
    protected $table = "meeting_schedule";
    protected $guarded = ['id'];

    public static function getMeetingMember(){
        $user = Auth::user();
        $company_id = $user->company_id;
        return User::where('company_id', $company_id)->get();
    }

    public static function getAllmeeting(){
        $user = Auth::user();
        $userId = $user->id;
        $data =  schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
        ->where('meeting_member.userId', $userId)
        ->where('meeting_schedule.status', 1)
        ->get();
          foreach($data as $key => $value){
            $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
            ->where('scheduleId', $value->scheduleId)
            ->join('users','users.id','meeting_member.userId')
            ->get();
            $value["members"] = $member;
        }
        return $data;
    }
    public static function acceptedSchedule(){
        $user = Auth::user();
        $userId = $user->id;
        $data =  schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
        ->where('meeting_member.accepted', 1)
        ->where('meeting_schedule.status', 1)    
        ->where('meeting_member.userId', $userId)    
        ->get();
            foreach($data as $key => $value){
            $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
            ->where('scheduleId', $value->scheduleId)
            ->join('users','users.id','meeting_member.userId')
            ->get();
            $value["members"] = $member;
        }
        return $data;
    }
    public static function acceptedScheduleByMonth($month, $year){
        $user = Auth::user();
        $userId = $user->id;
        $data = schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
                ->where('meeting_member.accepted', 1)
                ->where('meeting_schedule.status', 1)    
                ->where('meeting_member.userId', $userId)
                ->where('meeting_schedule.repeat', 'never')
                ->whereMonth('meeting_schedule.date', $month)    
                ->get()->toArray();
        $repeats = schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
                ->where('meeting_member.accepted', 1)
                ->where('meeting_schedule.status', 1)    
                ->where('meeting_member.userId', $userId)
                ->where('meeting_schedule.repeat', '!=', 'never')    
                ->get()->toArray();
        foreach($repeats as $repeat){

            /**
             * Logic if repeat meeting is everyday
             */

            if($repeat["repeat"] == 'everyday'){
                $monthDate = (int)date("m",strtotime($repeat["date"]));
                $yearDate = (int)date("Y",strtotime($repeat["date"]));
                if($monthDate == $month && $yearDate == $year){
                    array_push($data, $repeat);
                    $temp_month = date("m",strtotime($repeat["date"]));
                    while(date("m",strtotime($repeat["date"] . ' +1 day')) == $temp_month){
                                $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +1 day'));
                                $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +1 day'));
                                array_push($data, $repeat);
                    }
                }
                elseif(($month >= $monthDate&&(int)$year >= $yearDate)||($month < $monthDate&&(int)$year > $yearDate)){
                    $time = date("H:i:s",strtotime($repeat["date"]));
                    $endTime = date("H:i:s",strtotime($repeat["meetingEndTime"]));
                    $startdate = $year.'-'.$month.'-01 '.$time;
                    $endDate =$year.'-'.$month.'-01 '.$endTime;
                    $repeat["date"] = $startdate;
                    $repeat["meetingEndTime"] = $endDate;
                    array_push($data, $repeat);
                    while(date("m",strtotime( $repeat["date"]. ' +1 day')) == $month){
                        $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +1 day'));
                        $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +1 day'));
                        array_push($data, $repeat);
                    }
                }
            }


            /**
             * Logic if repeat meeting is weekly
             */

            if($repeat["repeat"] == 'weekly'){
                $monthDate = (int)date("m",strtotime($repeat["date"]));
                $yearDate = (int)date("Y",strtotime($repeat["date"]));
                if($monthDate == $month && $yearDate == $year){
                    array_push($data, $repeat);
                    $temp_month = date("m",strtotime($repeat["date"]));
                    while(date("m",strtotime($repeat["date"] . ' +7 day')) == $temp_month){
                                $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +7 day'));
                                $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +7 day'));
                                array_push($data, $repeat);
                    }
                }
                elseif(($month >= $monthDate&&(int)$year >= $yearDate)||($month < $monthDate&&(int)$year > $yearDate)){
                    $day = date("D",strtotime($repeat["date"]));
                    $time = date("H:i:s",strtotime($repeat["date"]));
                    $endTime = date("H:i:s",strtotime($repeat["meetingEndTime"]));

                    /**
                     * Start date convertion
                     */

                    $temp_startdate = $year.'-'.$month.'-'.$day.' '.$time;
                    $temp_startdate = \DateTime::createFromFormat('Y-m-D H:i:s', $temp_startdate);
                    $startdate = $temp_startdate->format('Y-m-d H:i:s');
                    while(date("m",strtotime($startdate . ' -7 day')) == $month){
                        $startdate = date('Y-m-d H:i:s',strtotime($startdate . ' -7 day'));
                    }
                    
                    /**
                     * End date convertion
                     */

                    $temp_endDate = $year.'-'.$month.'-'.$day.' '.$endTime;
                    $temp_endDate = \DateTime::createFromFormat('Y-m-D H:i:s', $temp_endDate);
                    $endDate = $temp_endDate->format('Y-m-d H:i:s');

                    /**
                     * Save new date to array
                     */

                    $repeat["date"] = $startdate;
                    $repeat["meetingEndTime"] = $endDate;
                    array_push($data, $repeat);

                    /**
                     * Push data to array based on meeting repetation
                     * Weekly = +7day
                     */

                    while(date("m",strtotime( $repeat["date"]. ' +7 day')) == $month){
                        $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +7 day'));
                        $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +7 day'));
                        array_push($data, $repeat);
                    }
                }
            }

            /**
             * Logic if repeat is everytwoweeks
             * Date +14days
             */

            if($repeat["repeat"] == 'everytwoweeks'){
                if(date("m",strtotime($repeat["date"])) == $month && date("Y",strtotime($repeat["date"])) == $year){
                    array_push($data, $repeat);
                    $temp_month = date("m",strtotime($repeat["date"]));
                    while(date("m",strtotime($repeat["date"] . ' +14 day')) == $temp_month){
                                $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +14 day'));
                                $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +14 day'));
                                array_push($data, $repeat);
                    }
                }
                elseif(date("m",strtotime($repeat["date"])) != $month ){
                    /**
                     * Get start date on the next month
                     */
                    $day = date("D",strtotime($repeat["date"]));
                    $time = date("H:i:s",strtotime($repeat["date"]));
                    $endTime = date("H:i:s",strtotime($repeat["meetingEndTime"]));

                    /**
                     * Start date convertion
                     */
    
                    $temp_startdate = $year.'-'.$month.'-'.$day.' '.$time;
                    $temp_startdate = \DateTime::createFromFormat('Y-m-D H:i:s', $temp_startdate);
                    $startdate = $temp_startdate->format('Y-m-d H:i:s');
                    
                    /**
                     * End date convertion
                     */

                    $temp_endDate = $year.'-'.$month.'-'.$day.' '.$endTime;
                    $temp_endDate = \DateTime::createFromFormat('Y-m-D H:i:s', $temp_endDate);
                    $endDate = $temp_endDate->format('Y-m-d H:i:s');

                    /**
                     * push new date to array
                     */
                    
                    $repeat["date"] = $startdate;
                    $repeat["meetingEndTime"] = $endDate;
                    array_push($data, $repeat);

                    /**
                     * Push data to array based on meeting repetation
                     * everytwoweeks = +14day
                     */

                    while(date("m",strtotime( $repeat["date"]. ' +14 day')) == $month){
                        $repeat["date"] = date('Y-m-d H:i:s',strtotime($repeat["date"] . ' +14 day'));
                        $repeat["meetingEndTime"] = date('Y-m-d H:i:s',strtotime($repeat["meetingEndTime"] . ' +14 day'));
                        array_push($data, $repeat);
                    }
                }
            }

        }
          foreach($data as $key => $value){
                    $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
                    ->where('scheduleId', $value["scheduleId"])
                    ->join('users','users.id','meeting_member.userId')
                    ->get();
                    $data[$key]["members"] = $member;
                }
        /**
         * Return alla data
         */

        return $data;
    }
    public static function rejectedSchedule(){
        $user = Auth::user();
        $userId = $user->id;
        $data = schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
        ->where('meeting_member.accepted', 0)
        ->where('meeting_schedule.status', 1)
        ->where('meeting_member.userId', $userId)        
        ->get();
                foreach($data as $key => $value){
            $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
            ->where('scheduleId', $value->scheduleId)
            ->join('users','users.id','meeting_member.userId')
            ->get();
            $value["members"] = $member;
        }
        return $data;
    }
    public static function detailSchedule($id){
        $user = Auth::user();
        $userId = $user->id;
        $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
        ->where('scheduleId', $id)
        ->join('users','users.id','meeting_member.userId')
        ->get();
        // return $member;
        $data = schedule::where('id', $id)        
        ->first();
        $data["members"] = $member;
        return $data;
    }
    protected static function unresponseSchedule(){
        $user = Auth::user();
        $userId = $user->id;
        
        // update noitf
        $getNotif = \DB::table('all_notifications')->where('about', 'meeting request')->where('status', 0)->where('receiver_id', $userId)->get();
        if(count($getNotif) > 0){
            foreach($getNotif as $notif){
                $updateNotif = \DB::table('all_notifications')->where('id', $notif->id)->update([
                    'status'=>1
                    ]);
            }
        }
        
        $data = schedule::join('meeting_member','meeting_member.scheduleId', 'meeting_schedule.id')
        ->where('meeting_member.accepted', NULL)
        ->where('meeting_schedule.status', 1)
        ->where('meeting_member.userId', $userId)        
        ->get();
        foreach($data as $key => $value){
            $member = scheduleMember::select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
            ->where('scheduleId', $value->scheduleId)
            ->join('users','users.id','meeting_member.userId')
            ->get();
            $value["members"] = $member;
        }
        
        return $data;
    }
    protected static function scheduleByDate($date){
            $data =schedule::select('id as sheduleId', 'title', 'description', 'date', 'repeat', 'company_id', 'meetingEndTime')
            ->whereDate('date', '=', $date)
            ->where('status', 1)
            ->first();
            if($data == null){
                return $data;
            }
            $meetingId = $data->sheduleId;
             $member = \DB::table('meeting_member')->select('meeting_member.userId', 'users.name', 'users.email', 'users.image', 'meeting_member.accepted')
            ->where('scheduleId', $meetingId)
            ->join('users','users.id','meeting_member.userId')
            ->get();
            $data["members"] = $member;
            return $data;
    }
}
