<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class overtimeWorks extends Model
{
    //
    protected $table = "overtime_work";
    protected $guarded = ['id'];

    protected static function getOvertime(){
        $userId = Auth::user()->id;
        $data = DB::table('overtime_work')->where('overtime_work.user_id',$userId)->get();
        return $data;
    }
}
