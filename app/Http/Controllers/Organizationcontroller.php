<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Organizationcontroller extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'legalName' => 'required',
            'companyPhone'=>'required',
            'companyEmail'=>'required',
            'companyWebsite'=>'required',
            'companyAddress'=>'required',
            'business_scope'=>'required',
            'package_id'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $company_code = \Str::random(8);
        // check company_code
        $check = DB::table('companies')->where('company_code', $company_code)->first();
        while($check !=null){
            $company_code = \Str::random(8);
            $check = DB::table('companies')->where('company_code', $company_code)->first();
        };
        $companyId = DB::table('companies')->insertGetId([
            'company_name' => $request->legalName,
            'company_phone' => $request->companyPhone,
            'company_email' => $request->companyEmail,
            'website' => $request->companyWebsite,
            'address' => $request->companyAddress,
            'business_scope_id' => $request->business_scope,
            'package_id' => $request->package_id,
            'company_code'=> $company_code
            ]);
        
        DB::table('users')->where('id', \Auth::id())->update([
            'company_id' => $companyId,
            'super_admin'=> 1
            ]);
        DB::table('attendance_settings')->insert([
                'company_id' => $companyId,
                'office_start_time' => '09:00:00',
                'office_end_time' => '18:00:00',
                'late_mark_duration' => 20
            ]);
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Create company success'
            ])->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function plans(){
        $plans = DB::table('plans_detail')->leftjoin('plans', 'plans_detail.plan_id', 'plans.id')->get();
        return response()->json(compact(
            'plans'
        ));
    }
    public function businessScope(){
        $businessScope = DB::table('business_scopes')->get();
        return response()->json(compact(
            'businessScope'
        ));
    }
}
