<?php

namespace App\Http\Controllers;
use App\scheduleMember;
use App\schedule;
use App\fcmKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helper\Reply;
use App\User;
use DB;

class meetingSchedule extends Controller
{
    //
    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required',
            'date' => 'required',
            'repeat' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        
        $userId = Auth::id();
        $companyId = Auth::user()->company_id;
        $meeting = new schedule();
        $meeting->title = $request->title;
        $meeting->description = $request->description;
        $meeting->date = $request->date;
        $meeting->repeat = $request->repeat;
        $meeting->user_id = \Auth::id();
        $meeting->company_id = $companyId;
        $meeting->meetingEndTime = $request->endTime;
        $meeting->status = 1;
        $meeting->save();

        $meetingMember = new scheduleMember();
        $meetingMember->scheduleId = $meeting->id;
        $meetingMember->userId = $userId;
        $meetingMember->accepted = NULL;
        $meetingMember->company_id = $companyId;
        $meetingMember->accepted = 1;
        $meetingMember->save();
        if($request->userId){
            
        $members = $request->userId;
        
        foreach ($members as $index => $name) {
            // return $name;
            $meetingMember = new scheduleMember();
            $meetingMember->scheduleId = $meeting->id;
            $meetingMember->userId = $name;
            $meetingMember->accepted = NULL;
            $meetingMember->company_id = $companyId;
            $meetingMember->save();
            
            $now = \Carbon\Carbon::now();
            $insertNotifReceiver = DB::table('all_notifications')->insert([
                'title'=>'You have a new schedule request',
                'slug_id'=>$meeting->id,
                'about'=>'meeting request',
                'receiver_id'=>$name,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId,
                ]);
        }
        /*Notif Here*/
        $user = \Auth::user();
        $follower = $request->userId;
        $recipients = fcmKey::with('user')->whereIn('user_id', $follower)->get();
        $recipients = $recipients->map(function ($recipients) {
                return $recipients->token;
        })->toArray();
        fcm()->to($recipients)->priority('high')->timeToLive(0)
            ->data([
                'content' => 'Meeting',
                'meetingId' => $meeting->id,
            ])
            ->notification([
                'title' => 'Meeting invitation',
                'body' => $user->name . ' mengundang anda dalam sebuah meeting',
            ])->send();
        /* End notif*/
        }
        return Reply::success(__('Create Schedule Success'));
    }
    public function edit(Request $request){
        $user = Auth::user();
        $userId = $user->id;
        $companyId = Auth::user()->company_id;
        schedule::where('id', $request->meetingId)
        ->update([
            'title' => $request->title,
            'description' => $request->description,
            'date' => $request->date,
            'meetingEndTime' => $request->endTime,
            'repeat' => $request->repeat,
            ]);
        $delete = scheduleMember::where('scheduleId', $request->meetingId)
                                ->delete();
        $meetingMember = new scheduleMember();
        $meetingMember->scheduleId = $request->meetingId;
        $meetingMember->userId = $userId;
        $meetingMember->accepted = 1;
        $meetingMember->company_id = $companyId;
        $meetingMember->save();
        if($request->userId){
        $members = $request->userId;
        
        foreach ($members as $index => $name) {
            // return $name;
            if($name != $userId){
               $meetingMember = new scheduleMember();
                $meetingMember->scheduleId = $request->meetingId;
                $meetingMember->userId = $name;
                $meetingMember->accepted = NULL;
                $meetingMember->company_id = $companyId;
                $meetingMember->save(); 
            }else{
                echo 'user';
            }
            
        }
        }
        return Reply::success(__('Edit success'));
    }
    public function delete(Request $request){
        schedule::where('id', $request->meetingId)
        ->update([
            'status' => 0
            ]);
        return Reply::success(__('Delete success'));
    }
    public function update(Request $request){
        $user = Auth::user();
        $userId = $user->id;
        // $meeting = scheduleMember::where('meetingId', $request->meetingId)->where('userId', $userId);
        if($request->status == 1){
            scheduleMember::where('scheduleId', $request->meetingId)
            ->where('userId', $userId)
            ->update(['accepted' => 1]);
            $message = 'Accept success';
            $pesan = 'menerima';
        }
        else{
            scheduleMember::where('scheduleId', $request->meetingId)
            ->where('userId', $userId)
            ->update(['accepted' => 0, 'rejectedReason' => $request->reason]);
            $message = 'Reject success';
            $pesan = 'menolak';
        }
            //Notif follow here
            $receiver = DB::table('meeting_schedule')->where('id', $request->meetingId)->first();
            $recipients = fcmKey::where('user_id', $receiver->user_id)->pluck('token')->toArray();
            fcm()->to($recipients)->priority('high')->timeToLive(0)
                ->data([
                    'content' => 'Meeting',
                    'meetingId' => $request->meetingId,
                ])
                ->notification([
                    'title' => 'Meeting Response',
                    'body' => $user->name.' '.$pesan.' permintaan meeting anda',
                ])->send();
            //End notif
            return Reply::success(__($message));
    }

    public function getAllUser(){
        $user = User::getAllUser();
        return response()->json(compact('user'));
    }

    public function getAllSchedule(){
        $data = schedule::getAllmeeting();
        return response()->json(compact('data'));
    }

    public function acceptedSchedule(Request $req){
        if($req->has('month') && $req->has('year')){
            $data = schedule::acceptedScheduleByMonth($req->month, $req->year);
            return response()->json(compact('data'));
        }
        $data = schedule::acceptedSchedule();
        return response()->json(compact('data'));
    }

    public function rejectedSchedule(){
        $data = schedule::rejectedSchedule();
        return response()->json(compact('data'));
    }
    public function unresponseSchedule(){
        $data = schedule::unresponseSchedule();
        return response()->json(compact('data'));
    }
    public function detailSchedule($id){
        $data = schedule::detailSchedule($id);
        return response()->json(compact('data'));
    }
    public function getScheduleBydate($date){
        $data = schedule::scheduleByDate($date);
        return response()->json(compact('data'));
    }
}
