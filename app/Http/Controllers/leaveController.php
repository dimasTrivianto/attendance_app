<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\leave;
use Illuminate\Support\Facades\Auth;
use App\Helper\Reply;

class leaveController extends Controller
{
    //
    public function leaveType(){
        $companyId = Auth::user()->company_id;
        $data = \DB::table('leave_types')->select('id', 'type_name')->where('company_id', $companyId)->orWhere('company_id', NULL)->get();
        return response()->json(compact('data'))->setStatusCode(200);
    }
    public function store(Request $request){
        $dateEnd = $request->leaveDateEnd;
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        $insert = leave::create([
            'company_id' => $companyId,
            'user_id' =>$userId,
            'leave_type_id' => $request->leaveTypeId,
            'duration' => $request->duration,
            'leave_date' => $request->leaveDate,
            'startTime' => $request->startTime,
            'endTime' => $request->endTime,
            'reason' => $request->reason,
            'leave_date_end' => $request->leaveDateEnd,
            'status' => 'pending'
        ]);
        return Reply::success(__('Success apply leave'));
    }
    public function index(){
        $data = leave::getLeave();
         return Reply::successWithData(__('Success'), compact('data'));
    }
    
    public function userLeave(){
        $getLeaves = leave::companyLeaves();
        return response()->json(compact('getLeaves'))->setStatusCode(200);
    }
    
    public function updateUserLeave(Request $request){
        $id = $request->id;
        $status = $request->status;
        $update = \DB::table('leaves')->where('id', $id)->update([
            'status'=>$status
        ]);
        if($status == 'accepted'){
            return response()->json([
            'Status'=>'Success',
            'Message'=>'status accepted'
            ]);
        }else {
            return response()->json([
            'Status'=>'Success',
            'Message'=>'status rejected'
            ]);
        }
        
    }
    
    public function perUserLeave(Request $request){
        $userId = $request->query('id');
        $data = leave::getLeaveTaken($userId);
        if(count($data) == 0){
            $data = 'null';
        }
        return response()->json(compact('data'))->setStatusCode(200);
    }
    
    public function perUserLeaveNo(Request $request){
        $userId= $request->query('id');
        $qtyLeave = \DB::table('user_leave')->where('user_leave.user_id', $userId)->join('leave_types', 'user_leave.leave_type_id', 'leave_types.id')->join('users', 'users.id', 'user_leave.user_id')->select('leave_types.type_name', 'user_leave.no_of_leaves', 'user_leave.id AS userLeaveId', 'users.id AS userId')->get();
        return response()->json(compact('qtyLeave'))->setStatusCode(200);
    }
    
    public function changeUserLeaveNo(Request $request){
        $id= $request->id;
        $number= $request->qty;
        $update = \DB::table('user_leave')->where('id', $id)->update([
            'no_of_leaves'=>$number    
        ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'number of leaves updated'
            ]);
    }
    
    public function changeAllLeaveNo(Request $request){
        $sick= $request->sick;
        $sickId= $request->sickId;
        $casual= $request->casual;
        $casualId= $request->casualId;
        $earned= $request->earned;
        $earnedId= $request->earnedId;
        $updateSick = \DB::table('user_leave')->where('id', $sickId)->update([
            'no_of_leaves'=>$sick   
        ]);
        $updateCasual = \DB::table('user_leave')->where('id', $casualId)->update([
            'no_of_leaves'=>$casual   
        ]);
        $updateSick = \DB::table('user_leave')->where('id', $earnedId)->update([
            'no_of_leaves'=>$earned   
        ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'number of leaves updated'
            ]);
    }
    
    public function userLeaveTaken(Request $request){
        $userId= $request->query('id');
        $cekTaken = \DB::table('leaves')->where('user_id', $userId)->where('status', 'approved')->count();
        $cekUntaken = \DB::table('user_leave')->where('user_id', $userId)->get();
        $qtyUntaken = 0;
        foreach($cekUntaken as $value){
            $qtyUntaken = $qtyUntaken + $value->no_of_leaves;
        }
        $qtyUntaken = $qtyUntaken - $cekTaken;
        $dataLeave = [$cekTaken,$qtyUntaken];
        return response()->json(compact('dataLeave'))->setStatusCode(200);
    }
}
