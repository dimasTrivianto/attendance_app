<?php

namespace App\Http\Controllers;

use App\User;
use App\attendance;
use App\fcmKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;
class processcontroller extends Controller
{
    public function messageType(){
        $getType = DB::table('message_type')->get();
        return response()->json(compact('getType'),200);
    }
    
    public function contactSupervisor(Request $request){
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        $now = \Carbon\Carbon::now();
        if($request->query('messageId')){
            $messageId = $request->query('messageId');
            $insert = DB::table('contact_supervisor_detail')->insert([
                'message'=>$request->message,
                'company_id'=>$companyId,
                'message_id'=>$messageId,
                 'sender_id'=>$userId,
                'receiver_id'=>$request->receiver_id,
                'created_at'=>$now,
                ]); 
                
            $insertNotifSender = DB::table('all_notifications')->insert([
                'title'=>'Your Message successfully send! See details here!',
                'slug_id'=>$messageId,
                'about'=>'contact supervisor',
                'receiver_id'=>$userId,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId
                ]);
                
            $insertNotifReceiver = DB::table('all_notifications')->insert([
                'title'=>'Your message has been replied See details here!',
                'slug_id'=>$messageId,
                'about'=>'contact supervisor',
                'receiver_id'=>$request->receiver_id,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId
                ]);
        }else{
           $insertGetId = DB::table('contact_supervisor')->insertGetId([
            'created_at'=>$now,
            'message_type_id'=>$request->type_id,
            'about'=>$request->about,
            ]);
            $insert = DB::table('contact_supervisor_detail')->insert([
                'message'=>$request->message,
                'company_id'=>$companyId,
                'message_id'=>$insertGetId,
                 'sender_id'=>$userId,
                'receiver_id'=>$request->receiver_id,
                'created_at'=>$now
                ]); 
            $insertNotifSender = DB::table('all_notifications')->insert([
                'title'=>'Your Message successfully send! See details here!',
                'slug_id'=>$insertGetId,
                'about'=>'contact supervisor',
                'receiver_id'=>$userId,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId
                ]);
            
            $insertNotifReceiver = DB::table('all_notifications')->insert([
                'title'=>'Your message has been replied See details here!',
                'slug_id'=>$insertGetId,
                'about'=>'contact supervisor',
                'receiver_id'=>$request->receiver_id,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId
                ]);
        }
        
        return response()->json([
            'status'=>'Success',
            'message'=>'Message has been sent'
            ]);
    }
    public function getContactSupervisor($id){
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        // get all the notif
        $getAllNotif = DB::table('all_notifications')->where('slug_id', $id)->where('receiver_id', $userId)->where('status', 0)->where('about', "contact supervisor")->get();
        if(count($getAllNotif) > 0){
            foreach($getAllNotif as $notif){
                $updateNotifStatus = DB::table('all_notifications')->where('id', $notif->id)->update([
                    'status'=>1
                ]);
            }
        }
        
        $getLastMessage = DB::table('contact_supervisor_detail')->where('message_id', $id)->where('sender_id', '!=', $userId)->where('status', 0)->get();
        if(count($getLastMessage) > 0){
            foreach($getLastMessage as $message){
                $update = DB::table('contact_supervisor_detail')->where('id', $message->id)->update([
                    'status'=>1
                    ]);
            }
        }
        $getAllMessage = DB::table('contact_supervisor_detail')->where('message_id', $id)->join('contact_supervisor', 'contact_supervisor.id', 'contact_supervisor_detail.message_id')->select('contact_supervisor_detail.message', 'contact_supervisor_detail.created_at AS messageTime', 'contact_supervisor_detail.status', 'contact_supervisor_detail.receiver_id', 'contact_supervisor_detail.sender_id', 'contact_supervisor.about')->orderBy('contact_supervisor_detail.created_at', 'ASC')->get();
        return response()->json(compact('getAllMessage'),200);
    }
    
    public function getMessageSupervisor(){
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        $getAllMessage = DB::table('contact_supervisor_detail')->where('contact_supervisor_detail.sender_id', $userId)->orWhere('contact_supervisor_detail.receiver_id', $userId)->join('contact_supervisor', 'contact_supervisor.id', 'contact_supervisor_detail.message_id')->select('contact_supervisor_detail.message_id', 'contact_supervisor_detail.sender_id', 'contact_supervisor_detail.receiver_id', 'contact_supervisor_detail.status','contact_supervisor_detail.created_at AS time', 'contact_supervisor.about')->get()->unique('message_id');
        $data = [];
        foreach($getAllMessage as $message){
            
            if($message->sender_id != $userId){
                $getUserData = DB::table('users')->where('company_id', $companyId)->where('id', $message->sender_id)->first();
                $data[]= [
                    'name'=>$getUserData->name,    
                    'photo'=>$getUserData->image,    
                    'about'=>$message->about,    
                    'time'=>$message->time,
                    'status'=>'New Message',
                    'messageId'=>$message->message_id
                ];
            }elseif($message->sender_id == $userId){
                if($message->status == 0){
                    $getUserData = DB::table('users')->where('company_id', $companyId)->where('id', $message->receiver_id)->first();
                    $data[]= [
                        'name'=>$getUserData->name,    
                        'photo'=>$getUserData->image,    
                        'about'=>$message->about,    
                        'time'=>$message->time,
                        'status'=>'Sent',
                        'messageId'=>$message->message_id
                    ];
                }else{
                    $getUserData = DB::table('users')->where('company_id', $companyId)->where('id', $message->receiver_id)->first();
                    $data[]= [
                        'name'=>$getUserData->name,    
                        'photo'=>$getUserData->image,    
                        'about'=>$message->about,    
                        'time'=>$message->time,
                        'status'=>'Read',
                        'messageId'=>$message->message_id
                    ];
                }
            }
        }
        return response()->json(compact('data'),200);
    }
    
    public function notif(){
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        // $notif
        $getNotif = DB::table('all_notifications')->where('receiver_id', $userId)->where('company_id',$companyId)->where('status', 0)->get();
        return response()->json(compact('getNotif'),200);
    }
    
    public function todayMeetingNotif(){
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        $now = date('Y-m-d');
        // get today meeting schedule
        $getAllSchedule = DB::table('meeting_member')->where('meeting_member.accepted', 1)->where('meeting_member.userId', $userId)->where('meeting_member.company_id', $companyId)->join('meeting_schedule', 'meeting_member.scheduleId', 'meeting_schedule.id')->get();
        foreach($getAllSchedule as $schedule){
            if($schedule->repeat == 'never'){
                // $id = $schedule->scheduleId;
                $date = strtotime($schedule->date);
                $date = date('Y-m-d', $date);
                
                // $date2 = \Carbon\Carbon::createFromFormat('Y-m-d', $date);
                // $date3 = $date2->addDays(7);
                // return response()->json(compact('date'),200);
                if($date == $now){
                    $insertNotif = DB::table('all_notifications')->insert([
                        'title'=>'You have meeting schedule Today!',
                        'slug_id'=>$schedule->scheduleId,
                        'about'=>'today meeting',
                        'receiver_id'=>$userId,
                        'status'=>0,
                        'created_at'=>$now,
                        'company_id'=>$companyId
                        ]);
                }
            }elseif($schedule->repeat == 'everyday'){
               $insertNotif = DB::table('all_notifications')->insert([
                        'title'=>'You have meeting schedule Today!',
                        'slug_id'=>$schedule->scheduleId,
                        'about'=>'today meeting',
                        'receiver_id'=>$userId,
                        'status'=>0,
                        'created_at'=>$now,
                        'company_id'=>$companyId
                        ]); 
            }elseif($schedule->repeat == 'weekly'){
                $date = strtotime($schedule->date);
                $date = date('Y-m-d', strtotime('+7days' , $date));
                if($now == $date){
                    $insertNotif = DB::table('all_notifications')->insert([
                        'title'=>'You have meeting schedule Today!',
                        'slug_id'=>$schedule->scheduleId,
                        'about'=>'today meeting',
                        'receiver_id'=>$userId,
                        'status'=>0,
                        'created_at'=>$now,
                        'company_id'=>$companyId
                        ]); 
                }
            }elseif($schedule->repeat == 'every two weeks'){
                $date = strtotime($schedule->date);
                $date = date('Y-m-d', strtotime('+14days' , $date));
                if($now == $date){
                    $insertNotif = DB::table('all_notifications')->insert([
                        'title'=>'You have meeting schedule Today!',
                        'slug_id'=>$schedule->scheduleId,
                        'about'=>'today meeting',
                        'receiver_id'=>$userId,
                        'status'=>0,
                        'created_at'=>$now,
                        'company_id'=>$companyId
                        ]); 
                }
            }elseif($schedule->repeat == 'every years'){
                $date = strtotime($schedule->date);
                $date = date('Y-m-d', strtotime('+1year' , $date));
                if($now == $date){
                    $insertNotif = DB::table('all_notifications')->insert([
                        'title'=>'You have meeting schedule Today!',
                        'slug_id'=>$schedule->scheduleId,
                        'about'=>'today meeting',
                        'receiver_id'=>$userId,
                        'status'=>0,
                        'created_at'=>$now,
                        'company_id'=>$companyId
                        ]); 
                }
            }
        }
        return response()->json(compact('addweek'),200);
    }
    
    public function dltContactSupervisor($id){
        $deleteContact = DB::table('contact_supervisor')->where('id',$id)->delete();
        $getAllMessage = DB::table('contact_supervisor_detail')->where('message_id', $id)->get();
        foreach($getAllMessage as $message){
            $getAllMessage = DB::table('contact_supervisor_detail')->where('id', $message->id)->delete();
            
        }
        return response()->json([
            'message'=>'Data has been deleted'
            ]);
    }
}
