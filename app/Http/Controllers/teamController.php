<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class teamController extends Controller
{
    //
    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'teamName' => 'required',
            'invitationLink'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        
        $teamId = \DB::table('teams')->insertGetId([
            'team_name' => $request->teamName,
            // 'invitation_link' => $request->invitationLink,
            // 'company_id' => $request->companyId
        ]);
        
        $getTeamData = \DB::table('teams')->where('id', $teamId)->first();
        $getTeamRoles = \DB::table('team_roles')->where('team_id', null)->where('roles_name', 'admin')->first();
        // return \Auth::id();
        \DB::table('employee_teams')->insert([
            'team_id' => $teamId,
            'team_role_id' => $getTeamRoles->id,
            'user_id' => \Auth::id()
            ]);
        // input validation link tanpa token
        $saveInvitationLink = $request->invitationLink;
        $updateTeam = \DB::table('teams')->where('id', $teamId)->update([
            'invitation_link'=> $saveInvitationLink
        ]);
        if($request->email1){
            $validator =  \Validator::make($request->all(), [
                'email1' => 'email',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }
            // return 'success';
            $email = $request->email1;

            $user = User::where('email', $email)->first();
            
            if($user != null){
                $token = JWTAuth::fromUser($user);
                $invitationLink = $request->invitationLink."token%3D".$token."%26teamId%3D".$teamId;
            }else{
                $invitationLink = $request->invitationLink."%26teamId%3D".$teamId;
            }
            $details = [
                'team_name'=>$getTeamData->team_name,
                'link'=>$invitationLink
            ];
            // return $invitationLink;
            $send = \Mail::to($email)->send(new \App\Mail\teamInvitation($details));
        }
        
        if($request->email2){
            $validator = \Validator::make($request->all(), [
                'email2' => 'email',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }
            $email = $request->email2;

            $user = User::where('email', $email)->first();
            
            if($user != null){
                $token = JWTAuth::fromUser($user);
                $invitationLink = $request->invitationLink."token%3D".$token."%26teamId%3D".$teamId;
            }else{
                $invitationLink = $request->invitationLink."%26teamId%3D".$teamId;
            }
            $details = [
                'team_name'=>$getTeamData->team_name,
                'link'=>$invitationLink
            ];

            $send = \Mail::to($email)->send(new \App\Mail\teamInvitation($details));
        }
        if($request->email3){
            $validator = \Validator::make($request->all(), [
                'email1' => 'email',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }
            $email = $request->email3;

            $user = User::where('email', $email)->first();
            
            if($user != null){
                $token = JWTAuth::fromUser($user);
                $invitationLink = $request->invitationLink."token%3D".$token."%26teamId%3D".$teamId;
            }else{
                $invitationLink = $request->invitationLink."%26teamId%3D".$teamId;
            }
            $details = [
                'team_name'=>$getTeamData->team_name,
                'link'=>$invitationLink
            ];

            $send = \Mail::to($email)->send(new \App\Mail\teamInvitation($details));
        }
        if($request->email4){
            $validator = \Validator::make($request->all(), [
                'email1' => 'email',
            ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 422);
            }
            $email = $request->email4;

            $user = User::where('email', $email)->first();
            
            if($user != null){
                $token = JWTAuth::fromUser($user);
                $invitationLink = $request->invitationLink."token%3D".$token."%26teamId%3D".$teamId;
            }else{
                $invitationLink = $request->invitationLink."%26teamId%3D".$teamId;
            }
            $details = [
                'team_name'=>$getTeamData->team_name,
                'link'=>$invitationLink
            ];

            $send = \Mail::to($email)->send(new \App\Mail\teamInvitation($details));
        }

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Create team success'
        ])->setStatusCode(200);
    }

    public function roles(){
        $data = \DB::table('team_roles')->get();
        return response()->json(compact('data'));
    }
    public function detail($id){
        $team = \DB::table('teams')->find($id);
        return response()->json(compact('team'), 200);
    }
    
    public function teamNew(Request $request){
        $validator = \Validator::make($request->all(), [
            'teamName' => 'required',
            'newMembers'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $companyId = \Auth::user()->company_id;
        
        $createNewTeams = \DB::table('teams')->insertGetId([
            'company_id'=>$companyId,
            'team_name'=>$request->teamName,
            'created_at'=> date('Y-m-d H:i:s')
        ]);
                
        $newMembers = $request->newMembers;
        
        foreach($newMembers as $member ){
            $changeTeam= \DB::table('employee_details')->updateOrInsert([
                    'user_id'=>$member
                ],
                [
                    'user_id'=>$member,
                    'department_id'=>$createNewTeams,
                    'company_id'=>$companyId,
                    'created_at'=> date('Y-m-d H:i:s')
                ]);
        }
        
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Create team success'
        ])->setStatusCode(200);
    }
    
    public function designationNew(Request $request){
        $validator = \Validator::make($request->all(), [
            'designationName' => 'required',
            'newMembers'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $companyId = \Auth::user()->company_id;
        $createNewDesignation = \DB::table('designations')->insertGetId([
            'company_id'=>$companyId,
            'name'=>$request->designationName,
            'created_at'=> date('Y-m-d H:i:s')
        ]);
                
        $newMembers = $request->newMembers;
        
        foreach($newMembers as $member ){
            $changeDesignation= \DB::table('employee_details')->updateOrInsert([
                'user_id'=> $member
                ],
                [
                    'user_id'=>$member,
                    'designation_id'=>$createNewDesignation,
                    'company_id'=>$companyId,
                    'created_at'=> date('Y-m-d H:i:s')
                ]);
        }
        
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Create designation success'
        ])->setStatusCode(200);
    }
    
    public function userRoles(){
         $company_id = \Auth::user()->company_id;
         $userRoles = \DB::table('roles')->where('company_id', $company_id)->get();
         return response()->json(compact('userRoles'), 200);
    }
    
    // public function teamPerUser(){
    //     $user = \Auth::user();
    //     $userId = $user->id;
    //     $currentTime = \Carbon\Carbon::now();
    //     $getEmployeeDetails = \DB::table('employee_details')->where('user_id', $userId)->first();
    //     $getDepartment = \DB::table('teams')->where('id', $getEmployeeDetails->department_id)->first();
    //     //  return response()->json(compact('getDepartment'), 200);
    //     $getDepartmentMembers = \DB::table('employee_details')->where('employee_details.department_id', $getDepartment->id)->join('users', 'users.id', 'employee_details.user_id')->select('users.id AS userId', 'users.name', 'users.')->get();
    //     $countMembers = count($getDepartmentMembers);
    //     $departement =[
    //          'name'=>$getDepartment->team_name,
    //          'countMembers'=>$countMembers
    //         ]; 
    //     foreach($getDepartmentMembers as $key=>$value){
    //         $memberAbsent = \DB::table('attendances')->where('user_id', $value->)
    //     }
    //     return response()->json(compact('departement'), 200);
    // }
    
    
}
