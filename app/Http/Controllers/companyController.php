<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\fcmKey;

class companyController extends Controller
{
    //
    public function companyCode($code){
        $company = \DB::table('companies')->select('id', 'company_name', 'logo', 'company_code')->where('company_code', $code)->first();
        return response()->json(compact('company'))->setStatusCode(200);
    }


    public function requestCompany(Request $request){
        $validator = \Validator::make($request->all(), [
            'companyCode' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $company = \DB::table('companies')->where('company_code', $request->companyCode)->first();
        if($company != NULL){
            //Insert company
            \DB::table('users')->where('id', \Auth::id())->update([
                'company_id' => $company->id
            ]);
            return response()->json([
                'Status' => 'Success',
                'Message' => 'Request success, waiting for aproval'
            ])->setStatusCode(200);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Request failed, company code does not exist'
        ])->setStatusCode(200);
    }
    public function requestCompanyWeb(Request $request){
        $validator = \Validator::make($request->all(), [
            'companyCode' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $company = \DB::table('companies')->where('company_code', $request->companyCode)->first();
        if($company != NULL){
            //Insert company
            \DB::table('users')->where('id', \Auth::id())->update([
                'company_id' => $company->id,
                'companyAcceptance'=>1,
                'company_id'=>$company->id
            ]);
            return response()->json($company)->setStatusCode(200);
        }
        return response()->json([
            'Status' => 'Failed',
            'Message' => 'Request failed, company code does not exist'
        ])->setStatusCode(200);
    }

    public function acceptUser(Request $request){
        $validator = \Validator::make($request->all(), [
            'userId' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        User::where('id', $request->userId)->update([
            'companyAcceptance' => 1,
        ]);


        /**
         * Notif for User if their request has been accepted
         */

         $recipients = fcmKey::where('user_id', $request->userId)->pluck('token')->toArray();
         fcm()->to($recipients)->priority('high')->timeToLive(0)
             ->data([
                 'companyAcceptance' => 1,
             ])
             ->notification([
                 'title' => 'Company Acceptance',
                 'body' => 'Your request for joining company has been accepted',
             ])->send();
        /**
         * End notif
         */

        return response()->json([
            'Status' => 'Success',
            'Message' => 'Your request for joining company has been accepted'
        ]);
    }

    // public function companyAllCode(){
    //      $company = \DB::table('companies')->select('id', 'company_name', 'logo', 'company_code')->get();
    //     return response()->json(compact('company'))->setStatusCode(200);
    // }
    
    public function companyMembers(Request $request) {
        $company_id =  $request->query('company_id');
        $companyMembers = \DB::table('users')->where('company_id', $company_id)->get();
        foreach($companyMembers as$key=>$member){
            $id = $member->id;
            $getRole = \DB::table('role_user')->where('user_id', $id)->join('roles', 'roles.id', 'role_user.role_id')->first();
            if($getRole!=null){
                $companyMembers[$key]->role= $getRole->name;
            }else{
                $companyMembers[$key]->role= null;
            }
        }
            return response()->json($companyMembers)->setStatusCode(200);
    }
    
    public function companyDepartment() {
        $companyId = \Auth::user()->company_id;
        $getDepartment = \DB::table('teams')->where('company_id', $companyId)->get();
        $departmentData = [];
        foreach($getDepartment as $department){
            $id = $department->id;
            // $companyDepartment = \DB::table('employee_details')->where('employee_details.company_id', $companyId)->where('employee_details.department_id', $department->id)->join('teams', 'teams.id', 'employee_details.department_id')->join('users', 'employee_details.user_id', 'users.id')->select('users.id', 'users.name', 'users.email', 'teams.name')->get();
            $countMembers = \DB::table('employee_details')->where('employee_details.company_id', $companyId)->where('employee_details.department_id', $department->id)->join('teams', 'teams.id', 'employee_details.department_id')->count();
            $departmentData[] = [
                'departmentName'=>$department->team_name,
                'departmentId'=>$id,
                'qtyMembers'=>$countMembers,
            ];
        }
            return response()->json(compact('departmentData'))->setStatusCode(200);
    }
    public function companyDesignation() {
        $companyId = \Auth::user()->company_id;
        $getDesignation = \DB::table('designations')->where('company_id', $companyId)->get();
        $designationData = [];
        foreach($getDesignation as $designation){
            $id = $designation->id;
            $countMembers = \DB::table('employee_details')->where('employee_details.company_id', $companyId)->where('employee_details.designation_id', $id)->join('designations', 'designations.id', 'employee_details.designation_id')->count();
            $designationData[] = [
                'designationName'=>$designation->name,
                'designationId'=>$id,
                'qtyMembers'=>$countMembers,
            ];
        }
            return response()->json(compact('designationData'))->setStatusCode(200);
    }
     public function deleteDesignation(Request $request) {
         $id = $request->id;
         $deleteDesignation = \DB::table('designations')->where('id', $id)->delete();
         $updateMemberDesignation = \DB::table('employee_details')->where('designation_id', $id)->update([
                'designation_id'=>null
            ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Designation has been deleted'
            ])->setStatusCode(200);
    }
    public function deleteDepartment(Request $request) {
         $id = $request->id;
         $deleteDepartment = \DB::table('teams')->where('id', $id)->delete();
         $updateMemberDesignation = \DB::table('employee_details')->where('department_id', $id)->update([
                'department_id'=>null
            ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Department has been deleted'
            ])->setStatusCode(200);
    }
    public function manageDesignation(Request $request) {
         $id = $request->id;
         $designations = \DB::table('employee_details')->where('employee_details.designation_id', $id)->join('designations', 'employee_details.designation_id', 'designations.id')->join('users', 'users.id',  'employee_details.user_id')->select('users.name', 'users.id as UserId', 'users.email', 'users.status', 'designations.name as DesignationsName', 'designations.id')->get();
         foreach ($designations as $key=>$people) {
            $id = $people->UserId;
             $getRole = \DB::table('role_user')->where('role_user.user_id', $id)->join('roles', 'roles.id', 'role_user.role_id')->first();
            $designations[$key]->role= $getRole->name;         
         }
        return response()->json(compact('designations'));
    }
    public function manageDepartment(Request $request) {
         $id = $request->id;
         $departmentData = \DB::table('teams')->where('id', $id)->first();
         $member = \DB::table('employee_details')->where('employee_details.department_id', $id)->join('users', 'users.id', 'employee_details.user_id')->select('users.name', 'users.id as UserId', 'users.email', 'users.status')->get();
        //  $department = \DB::table('employee_details')->where('employee_details.department_id', $id)->join('teams', 'teams.id', 'employee_details.department_id')->join('users', 'users.id', 'employee_details.user_id')->select('users.name', 'users.id as UserId', 'users.email', 'users.status', 'teams.team_name', 'teams.id')->get();
        // return response()->json(compact('member'));
        if(count($member) >= 1){
            foreach($member as $key=>$people){
             $id = $people->UserId;
             $getRole = \DB::table('role_user')->where('role_user.user_id', $id)->join('roles', 'roles.id', 'role_user.role_id')->first();
            $member[$key]->role= $getRole->name;
         }
        }else{
           $member = null;
        } 
         
        return response()->json(compact('member', 'departmentData'));
    }
    public function changeRole(Request $request){
        // $UserId = \Auth::user()->id;
        $data = $request->role;
        $splitData = explode(",", $data);
        $role = $splitData[0];
        $UserId = $splitData[1];
        $changeRole = \DB::table('role_user')->updateOrInsert([
                'user_id'=> $UserId
            ],
            [
                'user_id'=> $UserId,
                'role_id'=>$role
            ]);
        return response()->json(compact('changeRole'));
    }
    public function changeDepartment(Request $request){
        // $UserId = \Auth::user()->id;
        $data = $request->id;
        $newName = $request->newName;
        $changeDepartmentName = \DB::table('teams')->where('id', $data)->update([
            'team_name'=>$newName
            ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Department name has been deleted'
            ])->setStatusCode(200);
    }
    
    public function changeDesignation(Request $request){
        // $UserId = \Auth::user()->id;
        $data = $request->id;
        $newName = $request->newName;
        $changeDepartmentName = \DB::table('teams')->where('id', $data)->update([
            'team_name'=>$newName
            ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Department name has been deleted'
            ])->setStatusCode(200);
    }
    public function companyEdit(Request $request){
        $image = $request->file('picture');
        // $image = $request->file['picture'];
        
        $request= json_decode($request->data);
        // $companyName = $request->companyName;
      $company_id = \Auth::user()->company_id;
      $path =NULL;
            if($image){
                date_default_timezone_set("Asia/Jakarta");
                // $file = $request->file('picture');
                $uniqueFileName = str_replace(" ","",time()."-".$request->companyName.".png");            
                $path = $image->storeAs('public/companyProfile', $uniqueFileName);
                $path = "storage/companyProfile/".$uniqueFileName;
                $editCompany = \DB::table('companies')->where('id', $company_id)->update([
                    "company_name"=>$request->companyName,
                    "company_phone"=>$request->companyPhone,
                    "company_email"=>$request->companyEmail,
                    "website"=>$request->companyWebsite,
                    "address"=>$request->companyAddress,
                    "logo"=>$path,
                    "currency_id"=>$request->currency,
                    "date_format"=>$request->dateFormat,
                    "timezone"=>$request->timezone,
                    "week_start"=>$request->weekStart,
                    "time_format"=>$request->timeFormat,
                    "locale"=>$request->language,
                ]);
            }else{
                $editCompany = \DB::table('companies')->where('id', $company_id)->update([
                    "company_name"=>$request->companyName,
                    "company_phone"=>$request->companyPhone,
                    "company_email"=>$request->companyEmail,
                    "website"=>$request->companyWebsite,
                    "address"=>$request->companyAddress,
                    "currency_id"=>$request->currency,
                    "date_format"=>$request->dateFormat,
                    "timezone"=>$request->timezone,
                    "week_start"=>$request->weekStart,
                    "time_format"=>$request->timeFormat,
                    "locale"=>$request->language,
                ]);
            }
      
          return response()->json([
            'Status'=>'Success',
            'Message'=>'Company Setting is saved'
            ])->setStatusCode(200);
        //   return response()->json(compact('request'), 200);
    }
    
    public function currency(){
        $currency = \DB::table('global_currencies')->get();
        return response()->json(compact('currency'), 200);
    }
    public function language(){
        $language = \DB::table('language_settings')->get();
        return response()->json(compact('language'), 200);
    }
}
