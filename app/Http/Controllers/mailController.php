<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Guzzle\Http\Exception\ClientErrorResponseException;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Session;
use cookie;
use Mailgun\Mailgun;
use App\User;
use Illuminate\Support\Str;

require '../vendor/autoload.php';


class mailController extends Controller
{
    /**
     * First change your credentials on .env
     * @from fill with your email
     * @subject fill with your email's subject
     * @template fill with your template's name on mailgun dashboard
     * @var string
     */
    private $emailAccount = '';
    private $from = 'Zukses <support@zukses.com>';
    private $subject = 'Zukses Reset Password';
    private $template = 'Nama-template-disini';

    /**
     * Email reset password
     */
    public function sendMail(Request $req){
        $this->emailAccount = $req->email;
        $user = User::where('email', $this->emailAccount)->first();
        if(User::where('email', $this->emailAccount)->count() != 0){
            $token = $this->createUserToken($this->emailAccount, 'resetPassword');
            if($this->validateMail() == 'true'){
                /**Send mail disini */
                $mg = Mailgun::create(env('MAILGUN_SECRET'), 'https://api.eu.mailgun.net'); // For EU servers
                $params = array(
                    'from'    => $this->from,
                    'to'      => $this->emailAccount,
                    'subject' => $this->subject,
                    'template'    => $this->template,
                    'v:token'   => $token,
                    'h:X-Mailgun-Variables'    => '{"$token": "'.$token.'"}'
                  );
                $result = $mg->messages()->send(ENV('MAILGUN_DOMAIN'),$params);
                return response()->json([
                    'Status' => "Success",
                    'Data' => $result
                ]);
            }
            else{
                /**Mail cannot be validated */
                return response()->json([
                    'Status' => "Success",
                    'Message' => 'Alamat email gagal di validasi'
                ]);
            }
        }
        /**
         * Emal tidak ada di database
         */
        return response()->json([
            'Status' => "Success",
            'Message' => 'Alamat email tidak ditemukan'
        ]);
    }

/**
 * Check email using mailgun
 * Exist or not
 * @return void
 */
    public function validateMail(){
        $client = new Client();
        $result = $client->get('https://api.mailgun.net/v3/address/validate?address='.$this->emailAccount.'&api_key='.env('MAILGUN_PUBLIC_KEY').'&mailbox_verification=true');
        $res = json_decode($result->getBody());
        $check = $res->mailbox_verification;
        return $check;
    }
    
    public function createUserToken($email, $type = NULL){
        $token = Str::random(512);;
        if($type == 'resetPassword'){
            User::where('email', $email)->update([
                'token_forgot_pass' => $token
            ]);
            return $token;
        }
    }

}
