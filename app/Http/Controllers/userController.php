<?php

namespace App\Http\Controllers;

use App\User;
use App\attendance;
use App\fcmKey;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use JWTAuth;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        // return  $credentials;
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        if($request->validationLink){
            $validationLink = $request->validationLink;
            $teamId = explode("teamId%3D", $validationLink);
            // $token = explode("token%3D", $teamId[0]);
            $team_role_id = DB::table('team_roles')->where('team_id', null)->where('company_id', null)->first();
            $insertTeam = DB::table('employee_teams')->insert([
                "team_id"=>$teamId[1],
                "team_role_id"=>$team_role_id->team_role_id,
                "user_id"=>$user->id,
            ]);
        };
        
        $user = Auth::user();
        if($request->fcmToken){
            if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                fcmKey::create([
                    'user_id' => $user->id,
                    'token' => $request->fcmToken
                ]);
            }
        }
        $attendance=attendance::todayAttendance($user->id);
        $maxClockIn=attendance::maxClockIn($user->id);
        
        return response()->json(compact('token','user','attendance', 'maxClockIn'));
    }
    
    public function loginAdmin(Request $request)
    {
        
        $credentials = $request->only('email', 'password');
        // return  $credentials;
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        
        $user = Auth::user();
        $userRole = User::getUserRole();
        if($userRole->name != 'admin') {
            return response()->json(['error' => 'access denied'], 401);
        }
        
        $attendance=attendance::todayAttendance($user->id);
        $maxClockIn=attendance::maxClockIn($user->id);
        return response()->json(compact('token','user','attendance', 'maxClockIn', 'userRole'));
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);
        // return $user->id;
        if($request->validationLink){
            $validationLink = $request->validationLink;
            $teamId = explode("teamId%3D", $validationLink);
            // $token = explode("token%3D", $teamId[0]);
            $team_role_id = DB::table('team_roles')->where('team_id', null)->where('company_id', null)->first();
            $insertTeam = DB::table('employee_teams')->insert([
                "team_id"=>$teamId[1],
                "team_role_id"=>$team_role_id->team_role_id,
                "user_id"=>$user->id,
            ]);
        };
        // return $token[1];
        $token = \JWTAuth::fromUser($user);
        if($request->fcmToken){
            fcmKey::create([
                'user_id' => $user->id,
                'token' => $request->fcmToken
            ]);
        }
        return response()->json(compact('user','token'),201);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

            return response()->json(['token_expired'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

            return response()->json(['token_invalid'], $e->getStatusCode());

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

            return response()->json(['token_absent'], $e->getStatusCode());

        }

        return response()->json(compact('user'));
    }
    public function getUserData(){
        $user = Auth::user();
        $userId = $user->id;
        // $userId= 1;
        $user=\DB::table('users')->where('id',$userId)->first();
        $getDepartmentandDesignation = \DB::table('employee_details')->where('employee_details.user_id', $userId)->join('teams', 'teams.id', 'employee_details.department_id')->join('designations', 'designations.id', 'employee_details.designation_id')->first();
        if($getDepartmentandDesignation == null){
            $user->department = null;
            $user->designation = null;
        }else{
            $user->department = $getDepartmentandDesignation->team_name;
            $user->designation = $getDepartmentandDesignation->name;
        }
        return response()->json(compact('user'));
    }
    public function company(){
        $user = Auth::user();
        $companyId = $user->company_id;
        $company=\DB::table('companies')->where('companies.id',$companyId)->join('attendance_settings','attendance_settings.company_id', 'companies.id')->join('language_settings', 'companies.locale', 'language_settings.id')->join('global_currencies', 'companies.currency_id', 'global_currencies.id')
        ->select('companies.id AS companyId', 'companies.company_name', 'companies.company_code', 'companies.address', 'companies.company_email', 'companies.company_phone', 'companies.logo', 'companies.website', 'companies.currency_id', 'companies.timezone', 'companies.date_format', 'companies.time_format', 
        'companies.week_start', 'companies.locale', 'attendance_settings.id AS attendanceId', 'attendance_settings.company_id', 'attendance_settings.office_start_time', 'attendance_settings.office_end_time', 'attendance_settings.halfday_mark_time', 'attendance_settings.late_mark_duration', 'attendance_settings.clockin_in_day', 
        'attendance_settings.employee_clock_in_out', 'attendance_settings.office_open_days', 'attendance_settings.office_open_days', 'attendance_settings.ip_address', 'language_settings.id AS languageId', 'language_settings.language_code', 'language_settings.language_name','global_currencies.id AS currenciesId', 'global_currencies.currency_name', 
        'global_currencies.currency_symbol', 'global_currencies.currency_code')->first();
        return response()->json(compact('company'));
    }

    public function loginInvitation() {
        $teamId = explode("teamId%3D", $validationLink);
        $token = explode("token%3D", $teamId[0]);
    }
    
    public function editProfile(Request $request){
        
        //Save image
        $path =NULL;
            if($request->file('image')){
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $path = $file->storeAs('public/profile', $uniqueFileName);
             $path = "storage/profile/".$uniqueFileName;
            
            }
            else if($request->image == \Auth::user()->image){
                $path = $request->image;
            }
            else if($request->image){
                $base64_image = $request->image;
                $base64_image = "data:image/png;base64,".$base64_image;
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                    $file = base64_decode($data);
                    $uniqueFileName = "photos-clock-in-".time();            
                    \Storage::disk('local')->put("public/profile/".$uniqueFileName.".png", $file);
                    $path = "storage/profile/".$uniqueFileName.".png";
                }
            }
        //End save image
        \DB::table('users')->where('id', \Auth::id())->update([
            'mobile' => $request->phone,
            'image' => $path,
            'name' =>$request->name
            ]);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Success edit profile'
            ]);
    }

    public function forgotPassword(Request $request){
        $validator = Validator::make($request->all(), [
            'token' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        if(User::where('token_forgot_pass', $request->token)->count() != 0){
            User::where('token_forgot_pass', $request->token)->update([
                'password' => bcrypt($request->password),
                'token_forgot_pass' => NULL
            ]);
            return response()->json([
                'Status' => 'Success',
                'Message' => "Change password success"
            ]);
        }
        else{
            return response()->json([
                'Status' => 'Failed',
                'Message' => "Email verification expired"
            ]);
        }
    }

    public function googleSignIn(Request $request)
    {
            if (User::where('email', $request->email)->where('provider', $request->provider)->count() == 1) {
                        $validator = Validator::make($request->all(), [
                        'email' => 'required|string|email'
                    ]);
            
                    if ($validator->fails()) {
                        return response()->json($validator->errors(), 422);
                    }
                    //Login
                    $user = User::where('email', $request->email)->first();
                    $attendance=attendance::todayAttendance($user->id);
                    $maxClockIn=attendance::maxClockIn($user->id);
                    // get User Role
                    $userRole = \DB::table('role_user')->where('role_user.user_id', $user->id)->join('roles', 'roles.id', 'role_user.role_id')->select('roles.name')->first();
                    $token = \JWTAuth::fromUser($user);
                    if($request->fcmToken){
                        if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                            fcmKey::create([
                                'user_id' => $user->id,
                                'token' => $request->fcmToken
                            ]);
                        }
                    }
                    return response()->json(compact('user', 'token', 'userRole', 'attendance', 'maxClockIn'), 200);
            }
            /**
             * Email sudah terdaftar
             */
            if(User::where('email', $request->email)->count() == 1){
                return response()->json([
                    'Status' => "Failed",
                    'Message' => "Account already exist"
                ])->setStatuscode(409);
            }

            /**
             * Register new Email
             */
            $validator = Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'provider' => 'required',
            ]);
            if ($validator->fails()){
            return response()->json($validator->errors(), 422);
            }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'photo' => $request->link_image,
                'provider' => $request->provider
            ]);
            if($request->fcmToken){
                if(fcmKey::where('user_id', $user->id)->where('token', $request->fcmToken)->count()==0){
                    fcmKey::create([
                        'user_id' => $user->id,
                        'token' => $request->fcmToken
                    ]);
                }
            }
            $token = \JWTAuth::fromUser($user);
            $attendance=attendance::todayAttendance($user->id);
            $maxClockIn=attendance::maxClockIn();
            
            return response()->json(compact('token','user','attendance', 'maxClockIn'));
    }
    
    public function getUserProfile(Request $request){
        $memberId = $request->query('id');
        $getMemberProfile = \DB::table('employee_details')->where('employee_details.user_id', $memberId)->join('designations', 'designations.id', 'employee_details.designation_id')->join('teams', 'teams.id', 'employee_details.department_id')->join('users', 'users.id', 'employee_details.user_id')->select('users.name AS UserName', 'users.id', 'users.mobile', 'users.email', 'users.gender','employee_details.joining_date', 'employee_details.hourly_rate','designations.name','teams.team_name', 'users.login', 'users.email_notifications', 'users.image')->first();
        return response()->json(compact('getMemberProfile'), 200);
    }
    
    
    public function deleteUserProfile(Request $request){
        $userId = $request->id;
        $deleteUsers = \DB::table('users')->where('id', $userId)->delete();
        $deleteEmployeeDetails = \DB::table('employee_details')->where('user_id', $userId)->delete();
        return response()->json([
            'Status'=>'Success',
            'Message'=>'User has been deleted'
        ]);
    }
    
    public function updateUserProfile(Request $request){
        $id = $request->id;
        $name=$request->name;
        $gender=$request->gender;
        $mobile=$request->mobile;
        $email=$request->email;
        $designation=$request->designation;
        $department=$request->department;
        $joining=$request->joining;
        $salary=$request->salary;
        $updateUsers = \DB::table('users')->where('id',$id)->update([
            "name"=>$name,
            "email"=>$email,
            "mobile"=>$mobile,
            "gender"=>$gender,
            "login"=>$request->login,
            "email_notifications"=>$request->emailNotif,
        ]);
        $updateEmployeeDetails = \DB::table('employee_details')->where('user_id', $id)->update([
            "hourly_rate"=>$salary,  
            "joining_date"=>$joining,  
            "designation_id"=>$designation,
            "department_id"=>$department,
        ]);
        
        return response()->json(compact('id'), 200);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'User has been updated'
        ]);
    }
    
    public function getNewAccess(){
       $company_id = Auth::user()->company_id; 
       $getNewAccess = User::getNewUserCompany($company_id);
       return response()->json(compact('getNewAccess'), 200);
    }
    public function updateNewAccess(Request $request){
       $company_id = Auth::user()->company_id;
       $userId = $request->id;
       $status = $request->status;
       $getNewAccess = \DB::table('users')->where('id', $userId)->update([
            'companyAcceptance'=>$status
        ]);
        if($status == 1){
           return response()->json([
            'Status'=>'Success',
            'Message'=>'User access has been granted'
            ]); 
        }else{
            return response()->json([
            'Status'=>'Success',
            'Message'=>'User access request has been rejected'
            ]);
        }
    }
    public function profileChangesNotification(){
        $company_id = Auth::user()->company_id; 
        $getNotif = \DB::table('profile_notification')->where('profile_notification.company_id', $company_id)->where('profile_notification.status', 0)->join('users', 'profile_notification.user_id', 'users.id')->select('profile_notification.changes', 'profile_notification.id','users.id AS userId', 'users.name')->get();
        return response()->json(compact('getNotif'), 200);
    }
    
    public function updateProfileNotification(Request $request){
        $id = $request->id;
        $update = \DB::table('profile_notification')->where('id', $id)->update([
            'status'=>1
        ]);
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Updates status'
            ]);
    }
    
    public function addNewEmployee(Request $request){
        // $request = $request->login;
        // return response()->json(compact('request'), 200);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'id' => 'required|string',
            'joiningDate' => 'required',
            'designation' => 'required',
            'department' => 'required',
            'gender' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $date = \DateTime::createFromFormat('d/m/Y', $request->joiningDate);
        $joining_date = $date->format('Y-m-d H:i:s');
        $exit_date = NULL;
        $company_id = Auth::user()->company_id;
        $phone = NULL;
        $exitDate = NULL;
        // $gender = NULL;
        $address = NULL;
        $salary = NULL;
        $login = 'enable';
        $emailNotification = 1;
        $username = explode("@", $request->email);
        
        $path =NULL;
            if($request->file('picture')){
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('picture');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $path = $file->storeAs('public/profile', $uniqueFileName);
             $path = "storage/profile/".$uniqueFileName;
            
            }if($request->has('exitDate')){
                $exitDate = $request->exitDate;
            }if($request->has('phone')){
                $getExt = explode(",", $request->ext);
                $ext = $getExt[1];
                $phone = $ext.$request->phone;
            }if($request->has('address')){
                $address = $request->address;
            }if($request->has('salary')){
                $salary = $request->salary;
            }if($request->has('login')){
                $login = $request->login;
            }if($request->has('emailNotification')){
                $login = $request->emailNotification;
            }if($request->has('exitDate')){
                $exitDate = $exit_date;
            }
           
            $insertNewUser = \DB::table('users')->insertGetId([
                'company_id'=>$company_id,
                'name'=>$request->name,
                'username'=>$username[0],
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'image'=>$path,
                'mobile'=>$phone,
                'gender'=>$request->gender,
                'login'=>$login,
                'email_notifications'=>$emailNotification,
                'created_at'=>date('Y-m-d H:i:s')
            ]);
            //  return response()->json([
            // 'Status'=>'Success',
            // 'Message'=>'Employee added'
            // ]);
            $insertNewDetail = \DB::table('employee_details')->insert([
                'company_id' => $company_id,
                'user_id' => $insertNewUser,
                'slack_username'=>$username[0],
                'employee_id' => $request->id,
                'address' => $address,
                'hourly_rate' => $salary,
                'department_id' => $request->department,
                'designation_id' => $request->designation_id,
                'joining_date' => $joining_date,
                'last_date' => $exitDate,
                'created_at'=>date('Y-m-d H:i:s')
            ]);
            
            
        return response()->json([
            'Status'=>'Success',
            'Message'=>'Employee added'
            ]);
    }
    
        public function changePicture(Request $request, $user_id){
            // $userId = $request->query('id');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('photo');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $path = $file->storeAs('public/profile', $uniqueFileName);
             $path = "storage/profile/".$uniqueFileName;
             
        $updatePict = \DB::table('users')->where('id',$user_id)->update([
            'image'=>$path
        ]);
        return response()->json(compact('request'), 200);
    }
    public function filterMember(Request $request){
        $company_id = Auth::user()->company_id;
        if($request->query('page')=="people"){
            $companyMember = \DB::table('users')->where('users.company_id', $company_id)->join('employee_details', 'users.id', 'employee_details.user_id')->select('users.id', 'users.name', 'users.email', 'users.status', 'users.company_id', 'employee_details.designation_id', 'employee_details.department_id')->get();
        
        }
        if($request->query('page')=='designation'){
             $companyMember = \DB::table('employee_details')->where('employee_details.company_id', $company_id)->join('users', 'users.id', 'employee_details.user_id')->select('users.id', 'users.name', 'users.email', 'users.status','users.company_id', 'employee_details.designation_id')->get();
        }
        if($request->query('page')=='department'){
             $companyMember = \DB::table('employee_details')->where('employee_details.company_id', $company_id)->join('users', 'users.id', 'employee_details.user_id')->select('users.id', 'users.name', 'users.email', 'users.status','users.company_id', 'employee_details.department_id')->get();
        }
        foreach($companyMember as $key=>$value){
            $getRoles = \DB::table('role_user')->where('role_user.user_id', $value->id)->first();
            $companyMember[$key]->role = $getRoles->role_id;
            
        }

        if($request->filterEmployee != null){
            $companyMember = collect($companyMember)->filter(function ($value, $key) use($request) { return $value->id == $request->filterEmployee;
            });
        }
        if($request->filterStatus != null){
            $companyMember = collect($companyMember)->filter(function ($value, $key) use($request) { return $value->status == $request->filterStatus;
            });
        }
        if($request->filterRole != null){
            $companyMember = collect($companyMember)->filter(function ($value, $key) use($request) { return $value->role == $request->filterRole;
            });
        }
        if($request->filterDesignation != null){
            $companyMember = collect($companyMember)->filter(function ($value, $key) use($request) { return $value->designation_id == $request->filterDesignation;
            });
        }
        if($request->filterDepartment != null){
            $companyMember = collect($companyMember)->filter(function ($value, $key) use($request) { return $value->department_id == $request->filterDepartment;
            });
        }
        $request = ['employee'=>$request->filterEmployee,'status'=>$request->filterStatus, 'role'=>$request->filterRole, 'designation'=>$request->filterDesignation, 'department'=>$request->filterDepartment];
        return response()->json(compact(['companyMember', 'request']), 200);
    }
    
    public function notif(Request $request){
        $validator = Validator::make($request->all(), [
            'notif' => 'required|max:1',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        DB::table('users')->where('id', \Auth::id())->update([
            'notif' => $request->notif
        ]);

        return response()->json([
            'Status' => "Success",
            'Message' => 'Notif has been changed',
            'Notif' => $request->notif
            ]);
    }
    
    public function notifClockOut(){
        $allCompany = \DB::table('companies')->join('attendance_settings', 'companies.id', 'attendance_settings.company_id')->select('companies.id AS companyId','companies.timezone', 'attendance_settings.office_end_time')->get();
        foreach($allCompany as $key=>$value){
            $allMembers = \DB::table('users')->where('company_id', $value->companyId)->pluck('id');
            /**
             * Notif here
             */
             $currentTime= \Carbon\Carbon::now()->format('H:i');
             $clockOutTime = date('H:i',strtotime($value->office_end_time));
             if($currentTime == $clockOutTime){
                $recipients = fcmKey::with('user')->whereIn('user_id', $allMembers)->orderBy('id', 'DESC')->get()->unique('user_id');
                $array = [];
                foreach($recipients as $key=>$value){
                    array_push($array, $value->token);
                }
                // $recipients = $recipients->map(function ($recipients) {
                //     return $recipients->token;
                // })->toArray();
                fcm()->to($array)->priority('high')->timeToLive(0)
                    ->notification([
                        'body' => 'Don’t forget to clock out!',
                    ])->send();
            }
            
        }
        return response()->json([
            'Status' => "Success",
            'Message' => 'Notif has been sent',
            ]);
    }
    
    public function getUserAdmin(){
        $companyId = Auth::user()->company_id;
        $getAdminRole = DB::table('roles')->where('company_id', $companyId)->where('name', 'admin')->first();
        $getAdmin = DB::table('users')->where('users.company_id', $companyId)->join('role_user', 'users.id', 'role_user.user_id')->where('role_user.role_id', $getAdminRole->id)->get();
        return response()->json(compact('getAdmin'),200);
    }

}
