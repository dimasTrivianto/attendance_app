<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helper\Reply;
use App\attendance;
use Carbon\Carbon;
use App\User;
use App\attendanceSetting;
use App\overtimeWorks;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class memberAttendanceController extends Controller
{
    
    public function store(Request $request)
    {
        
        $now = Carbon::now();
        // $date = $now->gt($lateTime);
        // return $date;
        // return $now;
        // $userId = User::getUserByEmail($request->email);
        $user = Auth::user();
        $userId = $user->id;
        if($userId){
        // $userId=$userId->id;
        $clockInCount = attendance::getTotalUserClockIn($now->format('Y-m-d'), $userId);
        $companyId=\Auth::user()->company_id;
        $attendanceSetting = attendanceSetting::getAttendanceSetting($companyId);
        // Check user by ip
        // if ($attendanceSetting->ip_check == 'yes') {
        //     $ips = (array) json_decode($attendanceSetting->ip_address);
        //     if (!in_array($request->ip(), $ips)) {
        //         return Reply::error(__('messages.notAnAuthorisedDevice'));
        //     }
        // }

        // Check user by location
        // if ($attendanceSetting->radius_check == 'yes') {
        //     $checkRadius  = $this->isWithinRadius($request);
        //     if (!$checkRadius) {
        //         return Reply::error(__('messages.notAnValidLocation'));
        //     }
        // }

        // $attendanceSetting = AttendanceSetting::first();

        // Check maximum attendance in a day
        if ($clockInCount < $attendanceSetting->clockin_in_day) {

            // Set TimeZone And Convert into timestamp
            $currentTimestamp = $now->setTimezone('UTC');
            $currentTimestamp = $currentTimestamp->timestamp;

            // Set TimeZone And Convert into timestamp in halfday time
            if ($attendanceSetting->halfday_mark_time) {
                $halfDayTimestamp = $now->format('Y-m-d') . ' ' . $attendanceSetting->halfday_mark_time;
                // $halfDayTimestamp = Carbon::createFromFormat('Y-m-d H:i:s', $halfDayTimestamp, $this->global->timezone);
                $halfDayTimestamp = Carbon::createFromFormat('Y-m-d H:i:s', $halfDayTimestamp, 'UTC');
                $halfDayTimestamp = $halfDayTimestamp->setTimezone('UTC');
                $halfDayTimestamp = $halfDayTimestamp->timestamp;
            }

            $timeZone = User::getUserCompany($userId)->timezone;
            $timestamp = $now->format('Y-m-d') . ' ' . $attendanceSetting->office_start_time;
            $officeStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp);
            $officeStartTime = Carbon::createFromFormat('Y-m-d H:i:s', $timestamp, $timeZone );
            $officeStartTime = $officeStartTime->setTimezone('UTC');


            $lateTime = $officeStartTime->addMinutes($attendanceSetting->late_mark_duration);

            $checkTodayAttendance = attendance::where('user_id', $userId)
                ->where(DB::raw('DATE(attendances.clock_in_time)'), '=', $now->format('Y-m-d'))->first();
            $path = NULL;
            if($request->file('image')){
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ","",time()."-".$userId."-".$file->getClientOriginalName());            
            $path = $file->storeAs('public/photos', $uniqueFileName);
             $path = "storage/photos/".$uniqueFileName;
            
            }
            else if($request->image){
               
                $base64_image = $request->image;
                $base64_image = "data:image/png;base64,".$base64_image;
                
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                
                    $file = base64_decode($data);
                    $uniqueFileName = "photos-clock-in-".$userId.time();            

                    \Storage::disk('local')->put("public/photos/".$uniqueFileName.".png", $file);
                    $path = "storage/photos/".$uniqueFileName.".png";
              
                }
                
                
            }
            // return 'gak masuk keduanya';

            $attendance = new attendance();
            $attendance->user_id = $userId;
            $attendance->company_id = User::getUserCompany($userId)->id;            
            $attendance->clock_in_time = $now;
            $attendance->image = $path;
            $attendance->clock_in_ip = request()->ip();
            $attendance->clock_out_ip = '';
            $attendance->working_from = 'office';

            if ($now->gt($lateTime)) {
                $attendance->late = 'yes';
            }

            $attendance->half_day = 'no'; // default halfday

            // Check day's first record and half day time
            if (!is_null($attendanceSetting->halfday_mark_time) && is_null($checkTodayAttendance) && $currentTimestamp > $halfDayTimestamp) {
                $attendance->half_day = 'yes';
            }

            $attendance->save();
            $companies = \DB::table('companies')->where('id', $companyId)->first();
            date_default_timezone_set($companies->timezone);
            $time = date('h:i A');
            return Reply::successWithData(__('messages.attendanceSaveSuccess'), ['id'=>$attendance->id, 'time' => $time, 'ip' => $attendance->clock_in_ip, 'working_from' => $attendance->working_from]);
        }

        return Reply::error(__('messages.maxColckIn'));
        }
        else{
            return Reply::error(__('messages.userNotFound'));
        }
    }

      /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $now = Carbon::now('UTC');
        $user = Auth::user();
        $userId = $user->id;
        $attendance = attendance::getAttendanceByUserId($userId);
        if (is_null($attendance->clock_out_time)) {
            $attendance->clock_out_time = $now;
            $attendance->clock_out_ip = request()->ip();
            $attendance->save();
           return Reply::successWithData(__('messages.attendanceSaveSuccess'), ['id'=>$attendance->id,'ip' => $attendance->clock_in_ip, 'working_from' => $attendance->working_from]);
        }
        else{
            return Reply::error(__('messages.alreadyClockOutBefore'));
        }
    }

    public function check(Request $request){
        $user = Auth::user();
        $userId = $user->id;
        $attendance=attendance::todayAttendance($userId);
        return response()->json(compact('attendance'));
    }
    
    public function getUserAttendance($month, $year){
        $user = Auth::user();
        $userId = $user->id;
        // $userId = 239;
        $attendance=attendance::getUserAttendance($userId, $month, $year);
        return response()->json(compact('attendance'));
    }
    
    public function getUserAttendanceByday($date){
        $attendance=attendance::getUserAttendanceByDays($date);
        return response()->json(compact('attendance'));
    }

    public function overtimePost(Request $request){
        $validator = \Validator::make($request->all(), [
            'date'=>'required',
            'startTime'=>'required',
            'endTime'=>'required',
            'user_id'=>'required',
            'project'=>'required',
            'reason'=>'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $user = \Auth::user();
        $insert = overtimeWorks::create([
            'company_id' => $user->company_id,
            'user_id' => $user->id,
            'date' => $request->date,
            'startTime' => $request->startTime,
            'endTime' => $request->endTime,
            'project' => $request->project,
            'reason' => $request->reason
        ]);
        return Reply::success(__('Insert success'));
    }

    public function overtimeGet(Request $request){
        $data = overtimeWorks::getOvertime();
        return response()->json(compact('data'));
    }
    
    public function teamStatus(){
        $attendance=attendance::teamAttendance();
        $user = \Auth::user();
        $userId = $user->id;
        $departmentId = \DB::table('employee_teams')->where('user_id', $userId)->first()->team_id;
        $getTeams= \DB::table('teams')->where('teams.id', $departmentId)->join('employee_teams', 'teams.id', 'employee_teams.team_id')->get();
        $teamData = [
            'team_name'=>$getTeams[0]->team_name,
            'team_id'=>$departmentId,
            'countMembers'=>count($getTeams),
            ];
        return response()->json(compact(['attendance','teamData']));
    }  

    public function companyTodayAttendance(){
        $companyTodayAttendance = attendance::companyAttendance();
        return response()->json(compact('companyTodayAttendance'));
    }
    
    public function companyOvertime(){
        // $companyTodayAttendance = attendance::companyAttendance();
        // $dataOverWork = null;
        // if($companyTodayAttendance != null){
        //     $companyTimeZone = \DB::table('companies')->where('id', $companyTodayAttendance[0]['companyId'])->first();
        //     foreach($companyTodayAttendance as $member){
        //         $getOverWork = \DB::table('overtime_work')->where('attendanceId', $member['user_id'])->first();
        //         if($getOverWork !=null){
        //             $startHour = Carbon::createFromFormat('Y-m-d H:i:s', $value->startTime);
        //             $endHour = Carbon::createFromFormat('Y-m-d H:i:s', $value->endTime);
        //             $diffHour = $startHour->diffInHours($endHour);
        //             $dataOverWork[]=[
        //                 "id"=>$getOverWork->id,
        //                 "name"=>$member['name'],
        //                 "project"=>$getOverWork->project,
        //                 "reason"=>$getOverWork->reason,
        //                 "date"=>Carbon::parse($getOverWork->created_at, 'UTC')->setTimezone( $companyTimeZone->timezone)->format('d-m-Y'),
        //                 "duration"=>$diffHour
        //             ];
        //         }
        //     }
        // }
        $companyId=\Auth::user()->company_id;
        $companyTimeZone = \DB::table('companies')->where('id', $companyId)->first();
        $dataOverWork = null;
        $getOverWork = \DB::table('overtime_work')->where('overtime_work.company_id', $companyId)->where('overtime_work.status', 'pending')->join('users','overtime_work.user_id', 'users.id' )->select('users.name', 'overtime_work.id AS workId', 'overtime_work.project', 'overtime_work.reason', 'overtime_work.startTime', 'overtime_work.endTime', 'overtime_work.status', 'overtime_work.date')->get();
        if($getOverWork->count() >= 1){
           foreach($getOverWork as $key=>$value){
                $startHour = Carbon::createFromFormat('H:i:s', $value->startTime);
                $endHour = Carbon::createFromFormat('H:i:s', $value->endTime);
                $diffHour = $startHour->diffInHours($endHour);
               $dataOverWork[] = [
                    'id'=> $value->workId,
                    'name'=> $value->name,
                    'project'=> $value->project,
                    'reason'=> $value->reason,
                    'date'=> Carbon::parse($value->date, 'UTC')->setTimezone( $companyTimeZone->timezone)->format('d-m-Y'),
                    'duration'=> $diffHour
                ];
           }
        }else{
            $dataOverWork = null;
        }
        return response()->json(compact('dataOverWork'));
        
    }
    
    public function hoursLog(Request $request){
        $userId= $request->query('id');
        $hourLog = attendance::hourLog($userId);
        return response()->json(compact('hourLog'), 200);
    }
    
    public function companyAttendance($month, $year){
        $companyAllAttendance = attendance::companyAllAttendancesPerUser($month, $year);
        return response()->json(compact('companyAllAttendance'), 200);
    }
    
    public function companyDateAttendance(Request $request){
        $date = $request->date;
        $companyDateAttendance = attendance::companyDateAttendance($date);
        return response()->json(compact('companyDateAttendance'), 200); 
    }
    
    public function attendanceSetting(){
       $company_id = Auth::user()->company_id; 
       $getSettings = \DB::table('attendance_settings')->where('company_id', $company_id)->get();
       return response()->json(compact('getSettings'), 200);
    }
    public function editAttendanceSetting(Request $request){
       $company_id = Auth::user()->company_id; 
    //   $daysList = $request->office_open_days;
    //   $ipLists = $request->ipList;
       $days = null;
       $ipList = null;
       if($request->office_open_days != null){
            foreach($request->office_open_days as $key=>$item){
                if($key == 0 ){
                    $days = $item;
                }else{
                    $days = $days.','.$item;
                }
            }
       }
       
       if($request->ipList != null){
           foreach($request->ipList as $key=>$item){
                if($key == 0 ){
                    $ipList = $item;
                }else{
                    $ipList = $ipList.','.$item;
                }
            }
       }
      $editSettings = \DB::table('attendance_settings')->where('company_id', $company_id)->update([
          "office_start_time"=>$request->officeStart,
          "office_end_time"=>$request->officeEnd,
          "halfday_mark_time"=>$request->halfDay,
          "late_mark_duration"=>$request->lateMark,
          "clockin_in_day"=>$request->maxClockIn,
          "employee_clock_in_out"=>$request->selfClock,
          "office_open_days"=>$days,
          ]);
          return response()->json([
            'Status'=>'Success',
            'Message'=>'Attendance Setting is saved'
            ])->setStatusCode(200);
    //   return response()->json(compact('ipList'), 200);
    }
    
    public function attendanceByMember(Request $request){
        $id = $request->id;
        $dateStart = $request->dateStart;
        $startDate = date('Y-m-d', strtotime($request->dateStart));
        $endDate = date('Y-m-d', strtotime($request->dateEnd.'+1day'));
        $attendance=attendance::getUserAttendanceByMember($id, $startDate, $endDate);
        // $startdate = $request->dateStart;
        // $finishdate = $request->dateEnd;
        // $datetime1 = new \DateTime($startdate);
        // $datetime2 = new \DateTime($finishdate.'+1day');
        // $interval = $datetime1->diff($datetime2);
        // $days = $interval->format('%a');
        // for($i = 0; $i < $days; $i++){
        //     // $datetime1 = new \DateTime($startdate.'+'.$i'day');
        // };
        return response()->json(compact('attendance'));
    }
   

}
