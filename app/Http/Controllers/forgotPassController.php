<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use DB;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class forgotPassController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'password'=>'required|confirmed|string|min:6',
            'password_confirmation'=>'required|same:password',
            'token'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 422);
        }
        $token = $request->token;
        $newPass = bcrypt($request->password);
        $updateUserPass = DB::table('users')->where('token_forgot_pass', $token)->update([
            'password'=>$newPass,
            'token_forgot_pass'=>null
        ]);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Reset password success'
        ])->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function submitEmail(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email'=>'required',
            'link'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 422);
        }

        $email = $request->email;
        $link = $request->link;
        
        $user = User::where('email', $email)->first();
        $token = JWTAuth::fromUser($user);
        $link = $link."token%3D".$token;
        $details = [
            'name' => $user["name"],
            'link'=> $link
        ];
        $updateUser = DB::table('users')->where('email', $email)->update([
            'token_forgot_pass'=> $token
        ]);

        $send = \Mail::to($email)->send(new \App\Mail\forgotPassword($details));
        
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Email has been sent'
        ])->setStatusCode(200);

    }
}
