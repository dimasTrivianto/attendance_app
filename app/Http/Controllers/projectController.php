<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;
use App\Helper\Reply;
use App\User;
use App\task;
use App\taskAttachment;
use App\task_checklist;
use App\fcmKey;
use Carbon\Carbon;
class projectController extends Controller
{
    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'projectName' => 'required',
            'projectKey'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $path = NULL;
        if($request->file('image')){
            //  return 'gambar';
            // $request->file('file');
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $file->storeAs('public/project', $uniqueFileName);
            $path = "storage/project/".$uniqueFileName;
            }
            else if($request->image){
               
                $base64_image = $request->image;
                $base64_image = "data:image/png;base64,".$base64_image;
                
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                
                    $file = base64_decode($data);
                    $uniqueFileName = "photos-project-".time();            

                    \Storage::disk('local')->put("public/project/".$uniqueFileName.".png", $file);
                    $path = "storage/project/".$uniqueFileName.".png";
                }
            }
        $data = project::create([
            'company_id' => \Auth::user()->company_id,
            'project_name' => $request->projectName,
            'project_admin' => \Auth::id(),
            'notes' => $request->projectKey,
            'image' => $path
        ]);
        return response()->json([
            'Status' => 'Success',
            'data' => $data
        ], 200);
    }
    public function index(Request $request){
        // if(\Auth::user()->company_id != NULL){
            
        // }
        // else{

        // }
        if($request->has('projectId')){
            $data = \DB::table('tasks')
                    ->where('tasks.project_id', $request->projectId)
                    ->rightJoin('task_assignment', 'task_assignment.task_id', 'tasks.id')
                    ->get();
            return response()->json([
                'Status' => 'Success',
                'data' => $data
            ], 200);
        }
        $data = \DB::table('projects')
        ->select('projects.id', 'projects.project_name', 'projects.notes', 'projects.image',  \DB::raw(
            '(CASE
                WHEN COUNT(project_bookmarks.project_id) != 0 THEN 1
                WHEN COUNT(project_bookmarks.project_id) = 0 THEN 0
                END
            ) AS bookmark'))
        ->leftJoin('tasks', 'tasks.project_id', 'projects.id')
        ->leftJoin('project_bookmarks', 'project_bookmarks.project_id', 'projects.id')
        ->leftjoin('task_assignment', 'task_assignment.task_id', 'tasks.id')
        ->where('projects.project_admin', \Auth::id())
        ->orWhere('task_assignment.user_id', \Auth::id())
        // ->where(function ($query) {
        //         $query->where('task_assignment.user_id', \Auth::id())
        //               ->orWhere('projects.project_admin', \Auth::id());
        //     })
        ->groupBy('projects.id')
        ->orderBy('tasks.id', 'desc')
        ->get()->toArray();
        foreach($data as $key => $value){
            $count = \DB::table('tasks')
                    ->leftjoin('task_assignment', 'task_assignment.task_id', 'tasks.id')
                    ->where('project_id', $value->id)
                    ->where('task_assignment.user_id', \Auth::id())
                    ->where('tasks.progress', '!=', 'done')->count();
            $data[$key]->totalTask = $count;
        }
        return response()->json([
            'Status' => 'Success',
            'data' => $data
        ], 200);
    }

    public function createLabel(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        \DB::table('task_label')->insert([
            'name' => $request->name,
            'user_id' => \Auth::id(),
            'company_id' => \Auth::user()->company_id
        ]);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Input data berhasil'
        ], 200);
    }
    public function getLabel(){
        if(\Auth::user()->company_id != NULL){
            $data = \DB::table('task_label')->select('id', 'name')->where('user_id', \Auth::id())->orWhere('company_id', \Auth::user()->company_id)->get();
        }
        else{
            $data = \DB::table('task_label')->select('id', 'name')->where('user_id', \Auth::id())->get();
        }
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil diambil',
            'Data' => $data
        ], 200);
    }

    public function createTask(Request $request){
        $validator = \Validator::make($request->all(), [
            'projectId' => 'required',
            'title'=> 'required',
            'description'=> 'required',
            'priority'=> 'required',
            'dueDate' => 'required',
           
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $taskId = \DB::table('tasks')->insertGetId([
            'title' => $request->title,
            'project_id' => $request->projectId,
            'description' => $request->description,
            'priority' => $request->priority,
            'project_admin' => \Auth::id(),
            'due_date' => $request->dueDate,
            'notes' => $request->note,
            'user_id' => \Auth::id(),
            'company_id' => \Auth::user()->company_id,
            'label_id' => $request->labelId
        ]);
        if($request->assignTo){
            $members = $request->assignTo;
            foreach ($members as $index => $user) {
            // return User::find($user);
            $companyId = User::find($user)->company_id;

               \DB::table('task_assignment')->insert([
                   'task_id' => $taskId,
                   'user_id' => $user,
                   'company_id' => $companyId
               ]);
            $now = \Carbon\Carbon::now();
            $insertNotifReceiver = \DB::table('all_notifications')->insert([
                'title'=>"You have a new $request->priority Priority Task",
                'slug_id'=>$taskId,
                'about'=>'task',
                'receiver_id'=>$user,
                'status'=>0,
                'created_at'=>$now,
                'company_id'=>$companyId
                ]);
            }
            /**
             * Notif here
             */
            $user = \Auth::user();
            $follower = $request->assignTo;
            $recipients = fcmKey::with('user')->whereIn('user_id', $follower)->get();
            $recipients = $recipients->map(function ($recipients) {
                    return $recipients->token;
            })->toArray();
            fcm()->to($recipients)->priority('high')->timeToLive(0)
                ->data([
                    'content' => 'Task',
                    'projectId' => $request->projectId,
                    'taskId' => $taskId,
                ])
                ->notification([
                    'title' => 'Task assignment',
                    'body' => $user->name . ' menugaskan anda dalam sebuah task',
                ])->send();
            /* End notif */
        }
        return Reply::success(__('Success create task'));
    }
    
    public function editTask(Request $request){
        $validator = \Validator::make($request->all(), [
            'taskId' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $update = \DB::table('tasks')->where('id', $request->taskId)
                    ->update([
                        'priority' => $request->priority,
                        'due_date' => $request->dueDate,
                        'label_id' => $request->labelId
                        ]);
        return Reply::success(__('Update task success'));
    }

    public function taskProgress(Request $request){
        $validator = \Validator::make($request->all(), [
            'taskId' => 'required',
            'progress' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $update = \DB::table('tasks')->where('id', $request->taskId)
                    ->update([
                        'progress' => $request->progress
                        ]);
        return Reply::success(__('Update progress success'));
    }
    
    public function getTask(Request $request){
        $data = NULL;
        if($request->has('projectId')){
            // update task notif
            $updateNotif = \DB::table('all_notifications')->where('about', 'task')->where('status', 0)->where('receiver_id', \Auth::id())->where('slug_id', $request->projectId)->update([
                'status'=>1
                ]);
            
            $data = task::with('checklist')->with('assignment')->with('attachment')->with('reporter')
                    ->where('tasks.project_id', $request->projectId)
                    ->get();
        }
        if($request->has('projectId') && $request->has('progress')){
        $data = task::with('checklist')->with('assignment')->with('attachment')
                    ->where('tasks.project_id', $request->projectId)
                    ->where('tasks.progress', $request->progress)
                    ->get();
        }
        if($request->has('priority')){
                    $data = task::with('checklist')->with(['assignment' => function($query){
                        $query->orWhere('user_id', \Auth::id());
                    }])->with('attachment')
                    ->where('tasks.priority', $request->priority)
                    ->where('tasks.user_id', \Auth::id())
                    ->get();
        }
         return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil diambil',
            'Data' => $data
        ], 200);
    }
    
    public function getComment(Request $request){
        $data = NULL;
        if($request->has('taskId')){
            $data = \DB::table('task_comment')
            ->select('users.name', 'users.image', 'task_comment.*')
            ->join('users', 'users.id', 'task_comment.user_id')
            ->where('task_id', $request->taskId)
            ->get();
        }
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil diambil',
            'Data' => $data
        ], 200);
    }
    
    public function getAttachment(Request $request){
        $data = NULL;
        if($request->has('taskId')){
            $data = \DB::table('task_attachment')
            ->select('users.name', 'users.image', 'task_attachment.*')
            ->join('users', 'users.id', 'task_attachment.user_id')
            ->where('task_id', $request->taskId)
            ->get();
        }
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil diambil',
            'Data' => $data
        ], 200);
    }
    
    public function createComment(Request $request){
         $validator = \Validator::make($request->all(), [
            'taskId' => 'required',
            'comment'=> 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $id = \DB::table('task_comment')->insertGetId([
            'task_id' => $request->taskId,
            'user_id' => \Auth::id(),
            'comment' => $request->comment
            ]);
        $data = \DB::table('task_comment')->find($id);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil disimpan',
            'Data' => $data
        ], 200);
    }
    
    public function getChecklist(Request $request){
        $validator = \Validator::make($request->all(), [
            'taskId' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $data = task_checklist::where('task_id', $request->taskId)->get();
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil diambil',
            'Data' => $data
        ], 200);
    }

    public function deleteChecklist(Request $request){
        $validator = \Validator::make($request->all(), [
            'checklistId' => 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        task_checklist::where('id', $request->checklistId)->delete();
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil dihapus'
        ], 200);
    }
    
    public function createChecklist(Request $request){
        $validator = \Validator::make($request->all(), [
            'taskId' => 'required',
            'checklist'=> 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
         $id = \DB::table('task_checklist')->insertGetId([
            'task_id' => $request->taskId,
            'user_id' => \Auth::id(),
            'checklist' => $request->checklist
            ]);
        $data = \DB::table('task_checklist')->find($id);
        return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil disimpan',
            'Data' => $data
        ], 200);
    }
    
    public function editChecklist(Request $request){
        $validator = \Validator::make($request->all(), [
            'checklistId'=> 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        $id = $request->checklistId;
        if(\DB::table('task_checklist')->find($id)->status == 0){
           $data = \DB::table('task_checklist')->where('id', $id)->update([
            'status' => 1,
            'updated_at' => now()
            ]);
        }
        else{
             $data = \DB::table('task_checklist')->where('id', $id)->update([
            'status' => 0,
            'updated_at' => now()
            ]);
        }
        $data = \DB::table('task_checklist')->find($id);
         return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil ubah',
            'Data' => $data
        ], 200);
    }

    public function editChecklistName(Request $request){
        $validator = \Validator::make($request->all(), [
            'checklistId'=> 'required',
            'checklist' => 'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }

        task_checklist::where('id', $request->checklistId)->update([
            'checklist' => $request->checklist
        ]);
        
        $data = task_checklist::find($request->checklistId);
         return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil ubah',
            'Data' => $data
        ], 200);
    }
    
    public function attach(Request $request){
        $validator = \Validator::make($request->all(), [
            'image'=> 'required',
            'taskId'=> 'required',
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 422);
        }
        if($request->file('image')){
            date_default_timezone_set("Asia/Jakarta");
            $file = $request->file('image');
            $uniqueFileName = str_replace(" ","",time()."-".$file->getClientOriginalName());            
            $file->storeAs('public/attachment', $uniqueFileName);
            $path = "storage/attachment/".$uniqueFileName;
            }
            else if($request->image){
               
                $base64_image = $request->image;
                $base64_image = "data:image/png;base64,".$base64_image;
                
                if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {
                    $data = substr($base64_image, strpos($base64_image, ',') + 1);
                
                    $file = base64_decode($data);
                    $uniqueFileName = "photos-attachment-".time();            

                    \Storage::disk('local')->put("public/attachment/".$uniqueFileName.".png", $file);
                    $path = "storage/attachment/".$uniqueFileName.".png";
                }
            }
            
            $data = taskAttachment::create([
                    'user_id' => \Auth::id(),
                    'task_id' => $request->taskId,
                    'attachment' => $path
                ]);
            return response()->json([
            'Status' => 'Success',
            'Message' => 'Data berhasil disimpan',
            'Data' => $data
            ]);
        
    }
    
    public function allActiveProject() {
        $companyId = \Auth::user()->company_id;
        $allProjects = \DB::table('projects')->where('company_id',$companyId)->where('status', 'in Progress')->get();
        return response()->json(compact('allProjects'));;
    }
    public function allProject() {
        $companyId = \Auth::user()->company_id;
        $allProjects = \DB::table('projects')->where('company_id',$companyId)->get();
        return response()->json(compact('allProjects'));;
    }
    public function finishedProject() {
        $companyId = \Auth::user()->company_id;
        $finishedProjects = \DB::table('projects')->where('company_id', $companyId)->where('status', 'finished')->get();
        return response()->json(compact('finishedProjects'));
    }
    
    public function overdueTask() {
        $companyId = \Auth::user()->company_id;
        $companyEmployee = User::getUsersByCompanyId();
        $companyTimeZone = \DB::table('companies')->where('id', $companyId)->select('timezone')->first();
        $current_time = date('Y-m-d');
        foreach($companyEmployee as $employee){
            $getOverDueTask = \DB::table('task_assignment')->where('task_assignment.user_id', $employee->id)->join('tasks', 'tasks.id', 'task_assignment.task_id')->whereIn('tasks.progress', ['in-progress', 'To-Do'])->whereDate('tasks.due_date', '<=', $current_time)->join('projects','tasks.project_id', 'projects.id')->select('tasks.due_date', 'tasks.title', 'projects.project_name')->get();
            // return $getOverDueTask;
            if(count($getOverDueTask) >= 1){
                foreach($getOverDueTask as $task)
                $overDueTask[] = [
                'projectName'=>$task->project_name,
                'task'=>$task->title,
                'assignee'=>$employee->name,
                'dueDate'=>Carbon::parse($task->due_date)->format('d F Y'),
                // 'dueDate'=>Carbon::parse($task->due_date, 'UTC')->setTimezone( $companyTimeZone->timezone)->format('d F Y'),
                ];
            }else {
                $overDueTask = [];
            }
        }
        return response()->json(compact('overDueTask'));
    }
    public function overdueProject() {
        $companyId = \Auth::user()->company_id;
        $companyEmployee = User::getUsersByCompanyId();
        $companyTimeZone = \DB::table('companies')->where('id', $companyId)->select('timezone')->first();
        $current_time = date('Y-m-d');
        $overDueProject = [];
        foreach($companyEmployee as $employee){
            $getOverDueProject = \DB::table('projects')->where('projects.project_admin', $employee->id)->whereIn('projects.status', ['in progress', 'To-Do'])->whereDate('projects.deadline', '<=', $current_time)->join('project_client', 'project_client.project_id', 'projects.id')->select('projects.deadline', 'projects.completion_percent', 'projects.project_name', 'projects.status', 'project_client.client_name')->get();
            if(count($getOverDueProject) >= 1){
                foreach($getOverDueProject as $project)
                $overDueProject[] = [
                'projectName'=>$project->project_name,
                'completion'=>$project->completion_percent,
                'client'=>$project->client_name,
                'status'=>$project->status,
                'dueDate'=>Carbon::parse($project->deadline)->format('d F Y'),
                // 'dueDate'=>Carbon::parse($task->due_date, 'UTC')->setTimezone( $companyTimeZone->timezone)->format('d F Y'),
                ];
            }else {
                // $overDueProject = [];
            }
        }
        return response()->json(compact('overDueProject'));
    }
    
    public function notStarted(){
        $companyId = \Auth::user()->company_id;
        $current_time = date('Y-m-d');
        $getProjects = \DB::table('projects')->where('company_id', $companyId)->whereDate('start_date','>=', $current_time)->get(); 
        return response()->json(compact('getProjects'));
    }
    
    public function projectPerUser(Request $request){
        $id = $request->query('id');
        $getProjects = \DB::table('project_bookmarks')->where('project_bookmarks.user_id', $id)->join('projects', 'project_bookmarks.project_id', 'projects.id')->select('projects.project_name','projects.completion_percent','projects.status', 'projects.deadline')->get();
        return response()->json(compact('getProjects'));
    }
    
    public function taskPerUser(Request $request){
        $id = $request->query('id');
        $getTasks = \DB::table('task_assignment')->where('task_assignment.user_id', $id)->join('tasks', 'task_assignment.task_id', 'tasks.id')->select('tasks.title','tasks.due_date', 'tasks.project_id', 'tasks.progress')->get();
        foreach($getTasks as $key=>$task){
            $getProjectName = \DB::table('projects')->where('id', $task->project_id)->first();
            $getTasks[$key]->projectName=$getProjectName->project_name;
            $taskTimestamp = strtotime($task->due_date);
            $taskDate = date('d-m-Y', $taskTimestamp);
            $getTasks[$key]->due_date = $taskDate;
            if($task->progress == "To-Do" || $task->progress == "in-progress"){
                $getTasks[$key]->status = 'incomplete';
            }else{
                $getTasks[$key]->status = 'completed';
            }
        }
        return response()->json(compact('getTasks'));
    }
    
    public function updateOvertime(Request $request){
        $id = $request->id;
        $status = $request->status;
        $update = \DB::table('overtime_work')->where('id', $id)->update([
            'status'=>$status
        ]);
        if($status == 'accepted'){
            return response()->json([
            'Status'=>'Success',
            'Message'=>'status accepted'
            ]);
        }else{
            return response()->json([
            'Status'=>'Success',
            'Message'=>'status rejected'
            ]);
        }
    }
    public function taskDonePerUser(Request $request){
        $id = $request->query('id');
        $getDoneTasks = \DB::table('task_assignment')->where('task_assignment.user_id', $id)->join('tasks', 'task_assignment.task_id', 'tasks.id')->select('tasks.title','tasks.due_date', 'tasks.project_id', 'tasks.progress')->where('tasks.progress', 'done')->count();
        return response()->json(compact('getDoneTasks'));
    }
    public function allUnfinished(){
        $user_id = \Auth::id();
        $now = date('Y-m-d');
        $getAllUnfinished = \DB::table('task_assignment')->where('task_assignment.user_id', $user_id)->join('tasks', 'task_assignment.task_id', 'tasks.id')->where('tasks.progress', '!=', 'done')->count();
        $getAllSchedule = \DB::table('meeting_member')->where('meeting_member.userId', $user_id)->where('meeting_member.accepted', 1)->join('meeting_schedule', 'meeting_schedule.id', 'meeting_member.scheduleId')->where('date', $now)->count();
        return response()->json(compact(['getAllUnfinished','getAllSchedule']), 200);
    }

}
