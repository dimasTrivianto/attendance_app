<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Support\Facades\DB;
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="users";

    protected $guarded = [
        'id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','token_forgot_pass'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    protected function getUserByEmail($email){
        $user=DB::table('users')->select('id','name','email','image','gender', 'company_id')->where('email',$email)->first();
        return $user;
    }
    protected static function getUserCompany($id){
        $companyId = DB::table('users')->select('company_id')->where('id',$id)->first()->company_id;
        return DB::table('companies')->where('id',$companyId)->first();
    }
    
    protected static function getUsersByCompanyId(){
        $companyId = Auth::user()->company_id;
        $companyEmployee = DB::table('users')->where('company_id',$companyId)->get();
        return $companyEmployee;
    }

    public static function getAllUser(){
        $userId = Auth::user()->id;
        $companyId = DB::table('users')->select('company_id')->where('id',$userId)->first()->company_id;
        if($companyId != null){
        return DB::table('users')
        ->select('id','name', 'email', 'image')
        ->where('company_id', $companyId)
        ->where('users.id', '!=', auth()->id())
        ->get();
        }
        $team = DB::table('employee_teams')->where('user_id',$userId)->first();
        if($team == NULL ){
          return $team;            
        }
        $teamId = $team->team_id;
        return DB::table('employee_teams')
        ->join('users', 'users.id', 'employee_teams.user_id')
        ->select('users.id', 'users.name', 'users.email', 'users.image')
        ->where('employee_teams.team_id', $teamId)
        ->where('users.id', '!=', auth()->id())
        ->get();
    }

    public static function getUserRole() {
        $userId = Auth::user()->id;
        $companyId = Auth::user()->company_id;
        // $roles = DB::table('users')->where('users.company_id', $companyId)->join('role_user', 'users.id', 'role_user.user_id')->join('roles', 'roles.id', 'role_user.role_id')->where('roles.name', 'admin')->select('roles.name AS rolesName')->first();
        $roles = DB::table('role_user')->where('role_user.user_id', $userId)->join('roles', 'roles.id', 'role_user.role_id')->first();
        return $roles;
    }
    
    protected static function getNewUserCompany($id){
        $newAccess = DB::table('users')->where('company_id',$id)->where('companyAcceptance', 0)->select('users.id', 'users.name')->get();
        return $newAccess;
    }
   
}
