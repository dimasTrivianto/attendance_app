<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taskAttachment extends Model
{
    protected $table = 'task_attachment';
    
    protected $guarded = [];
    
    public function task(){
        return $this->belongsTo(\App\task::class,'task_id');
    }
}
