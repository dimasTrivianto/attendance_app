<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attendanceSetting extends Model
{
    //
    protected $table= 'attendance_settings';

    public static function getAttendanceSetting($companyId){
       return attendanceSetting::where('company_id',$companyId)->first();
    }
}
