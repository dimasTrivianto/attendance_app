<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class leave extends Model
{
    //
    protected $guarded = ['id'];

    protected function getLeave(){
        $userId = Auth::user()->id;
        return leave::where('leaves.user_id', $userId)
        ->select('leaves.id', 'companies.company_name', 'leave_types.type_name', 'leaves.duration', 'leaves.leave_date', 'leaves.startTime', 'leaves.endTime', 'leaves.reason', 'leaves.status','leaves.reject_reason')
        ->join('leave_types','leave_types.id', 'leaves.leave_type_id')
        ->join('companies','companies.id','leaves.company_id')
        ->get();
    }
    
    public static function companyLeaves(){
        $companyId = Auth::user()->company_id;
        $getLeaves = \DB::table('leaves')->where('leaves.company_id', $companyId)->where('leaves.status', 'pending')->join('users', 'leaves.user_id', 'users.id')->join('leave_types', 'leaves.leave_type_id', 'leave_types.id')->select('users.name', 'leaves.user_id AS userId', 'leaves.leave_date', 'leaves.reason', 'leave_types.type_name', 'leaves.id')->get();
        return $getLeaves;
    }
    protected function getLeaveTaken($userId){
        return leave::where('leaves.user_id', $userId)
        ->where('leaves.status', 'approved')
        ->select('leaves.id', 'companies.company_name', 'leave_types.type_name', 'leaves.duration', 'leaves.leave_date', 'leaves.startTime', 'leaves.endTime', 'leaves.reason', 'leaves.status','leaves.reject_reason')
        ->join('leave_types','leave_types.id', 'leaves.leave_type_id')
        ->join('companies','companies.id','leaves.company_id')
        ->get();
    }
    
}
