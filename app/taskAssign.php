<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class taskAssign extends Model
{
    protected $table = 'task_assignment';
    
    protected $guarded = ['id'];
    
    protected $appends = ['name', 'userPhoto'];
    
    protected $hidden = ['user'];
    
    public function task(){
        return $this->belongsTo(\App\task::class,'task_id');
    }
    public function user(){
        return $this->belongsTo(\App\User::class,'user_id');
    }
    public function getnameAttribute(){
        return $this->user->name;
    }
    public function getuserPhotoAttribute(){
        return $this->user->image;
    }
}
