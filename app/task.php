<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $table = 'tasks';
    
    protected $guarded = [];
    
    protected $appends = ['labelsName'];
    
    protected $hidden = ['label'];
    
    public function checklist(){
       return $this->hasMany(\App\task_checklist::class,'task_id');
    }
    
    public function attachment(){
       return $this->hasMany(\App\taskAttachment::class,'task_id');
    }
    
    public function assignment(){
       return $this->hasMany(\App\taskAssign::class,'task_id');
    }
    public function label(){
        return $this->belongsTo(\App\label::class,'label_id');
    }
    public function reporter(){
        return $this->belongsTo(\App\User::class,'project_admin');
    }
    public function getLabelsNameAttribute(){
        if($this->label){
          return $this->label->name;   
        }
       else return NULL; 
    }
    
}
